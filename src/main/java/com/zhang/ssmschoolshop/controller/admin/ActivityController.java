package com.zhang.ssmschoolshop.controller.admin;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zhang.ssmschoolshop.entity.Activity;
import com.zhang.ssmschoolshop.entity.ActivityExample;
import com.zhang.ssmschoolshop.entity.Admin;
import com.zhang.ssmschoolshop.entity.Goods;
import com.zhang.ssmschoolshop.service.ActivityService;
import com.zhang.ssmschoolshop.service.GoodsService;
import com.zhang.ssmschoolshop.util.Msg;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * 促销活动控制器
 *
 * 处理与促销活动相关的请求，包括展示活动信息、添加活动、更新商品活动关联、删除活动等操作。
 *
 * 使用说明：
 * 1. @Controller: 表示该类是一个Spring MVC的控制器。
 * 2. @RequestMapping: 映射请求路径，指定处理该控制器的请求路径前缀。
 * 3. @Autowired: 自动注入依赖，用于注入ActivityService和GoodsService。
 * 4. @ResponseBody: 表示方法的返回值直接写入HTTP响应体，而不是返回一个视图。
 *
 * 注意：
 * 1. 需要在Spring配置文件中配置相应的Bean。
 * 2. 页面跳转使用字符串形式，需要配置视图解析器。
 * 3. 使用前需登录管理员账户。
 */
@Controller
@RequestMapping("/admin/activity")
@Api(value = "促销活动controller", tags = {"促销活动操作接口"})
public class ActivityController {

    @Autowired(required = false)
    ActivityService activityService;

    @Autowired(required = false)
    GoodsService goodsService;

    /**
     * 展示促销活动信息
     *
     * @param pn      当前页码，默认为1
     * @param model   Spring MVC的Model对象，用于设置视图中的数据
     * @param session HTTP会话对象，用于获取管理员信息
     * @return 展示促销活动信息的视图名称
     */
    @RequestMapping("/show")
    public String showActivity(@RequestParam(value = "page", defaultValue = "1") Integer pn, Model model, HttpSession session) {
        Admin admin = (Admin) session.getAttribute("admin");
        if (admin == null) {
            return "redirect:/admin/login";
        }

        //一页显示几个数据
        PageHelper.startPage(pn, 10);

        ActivityExample activityExample = new ActivityExample();
        activityExample.or();

        List<Activity> activityList = activityService.getAllActivity(activityExample);

        //显示几个页号
        PageInfo page = new PageInfo(activityList, 5);
        model.addAttribute("pageInfo", page);

        return "activity";
    }

    /**
     * 以JSON格式返回促销活动信息
     *
     * @param pn      当前页码，默认为1
     * @param model   Spring MVC的Model对象，用于设置视图中的数据
     * @param session HTTP会话对象，用于获取管理员信息
     * @return 包含促销活动信息的Msg对象
     */
    @RequestMapping("/showjson")
    @ResponseBody
    public Msg showActivityJson(@RequestParam(value = "page", defaultValue = "1") Integer pn, Model model, HttpSession session) {
        Admin admin = (Admin) session.getAttribute("admin");
        if (admin == null) {
            return Msg.fail("请先登录");
        }

        ActivityExample activityExample = new ActivityExample();
        activityExample.or();

        List<Activity> activityList = activityService.getAllActivity(activityExample);

        return Msg.success("获取活动信息成功").add("activity", activityList);
    }

    /**
     * 跳转至添加促销活动页面
     *
     * @param session HTTP会话对象，用于获取管理员信息
     * @return 添加促销活动页面的视图名称
     */
    @RequestMapping("/add")
    public String showAddActivity(HttpSession session) {
        Admin admin = (Admin) session.getAttribute("admin");
        if (admin == null) {
            return "redirect:/admin/login";
        }
        return "addActivity";
    }

    /**
     * 添加促销活动
     *
     * @param activity 促销活动对象，包含添加的活动信息
     * @return 重定向到展示促销活动信息的视图
     */
    @RequestMapping("/addResult")
    public String addActivity(Activity activity) {
        activityService.insertActivitySelective(activity);
        return "redirect:/admin/activity/show";
    }

    /**
     * 更新商品与活动的关联关系
     *
     * @param goodsid    商品ID
     * @param activityid 活动ID
     * @param session    HTTP会话对象，用于获取管理员信息
     * @return 包含更新结果的Msg对象
     */
    @RequestMapping("/update")
    @ResponseBody
    public Msg updateActivity(Integer goodsid, Integer activityid, HttpSession session) {
        Admin admin = (Admin) session.getAttribute("admin");
        if (admin == null) {
            return Msg.fail("请先登录");
        }

        Goods goods = new Goods();
        goods.setActivityid(activityid);
        goods.setGoodsid(goodsid);
        goodsService.updateGoodsById(goods);

        return Msg.success("更新商品活动成功");
    }

    /**
     * 删除促销活动
     *
     * @param activityid 活动ID
     * @param session    HTTP会话对象，用于获取管理员信息
     * @return 重定向到展示促销活动信息的视图
     */
    @RequestMapping("delete")
    public String deleteActivity(Integer activityid, HttpSession session) {
        Admin admin = (Admin) session.getAttribute("admin");
        if (admin == null) {
            return "redirect:/admin/login";
        }

        activityService.deleteByActivityId(activityid);
        return "redirect:/admin/activity/show";
    }
}

