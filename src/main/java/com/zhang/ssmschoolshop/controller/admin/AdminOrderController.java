package com.zhang.ssmschoolshop.controller.admin;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zhang.ssmschoolshop.entity.*;
import com.zhang.ssmschoolshop.service.EmailService;
import com.zhang.ssmschoolshop.service.GoodsService;
import com.zhang.ssmschoolshop.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * 后台管理系统订单控制器
 *
 * 处理管理员对待发货订单的查看和相关操作。
 *
 * 使用说明：
 * 1. @Controller: 表示该类是一个Spring MVC的控制器。
 * 2. @RequestMapping: 映射请求路径，指定处理该控制器的请求路径前缀。
 * 3. @Autowired: 自动注入依赖，用于注入OrderService、GoodsService和EmailService。
 *
 * 注意：
 * 1. 需要在Spring配置文件中配置相应的Bean。
 * 2. 页面跳转使用字符串形式，需要配置视图解析器。
 * 3. 使用前需登录管理员账户。
 */
@Controller
@RequestMapping("/admin/order")
public class AdminOrderController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private GoodsService goodsService;

    @Autowired
    private EmailService emailService;

    /**
     * 查看待发货订单
     *
     * @param pn      当前页码，默认为1
     * @param model   Spring MVC的Model对象，用于设置视图中的数据
     * @param session HTTP会话对象，用于获取管理员信息
     * @return 待发货订单页面的视图名称
     */
    @RequestMapping("/send")
    public String sendOrder(@RequestParam(value = "page", defaultValue = "1") Integer pn, Model model, HttpSession session) {
        // 管理员登录验证
        Admin admin = (Admin) session.getAttribute("admin");
        if (admin == null) {
            return "redirect:/admin/login";
        }

        // 分页显示，一页显示2个订单
        PageHelper.startPage(pn, 2);

        // 查询未发货订单
        OrderExample orderExample = new OrderExample();
        orderExample.or().andIssendEqualTo(false);
        List<Order> orderList = orderService.selectOrderByExample(orderExample);
        model.addAttribute("sendOrder", orderList);

        // 查询该订单中的商品
        for (int i = 0; i < orderList.size(); i++) {
            // 获取订单项中的goodsid
            Order order = orderList.get(i);
            OrderItemExample orderItemExample = new OrderItemExample();
            orderItemExample.or().andOrderidEqualTo(order.getOrderid());
            List<OrderItem> orderItemList = orderService.getOrderItemByExample(orderItemExample);
            List<Integer> goodsIdList = new ArrayList<>();

            List<Goods> goodsList = new ArrayList<>();
            for (OrderItem orderItem : orderItemList) {
                Goods goods = goodsService.selectById(orderItem.getGoodsid());
                goods.setNum(orderItem.getNum());
                goodsList.add(goods);
            }

            // 设置订单中的商品信息
            order.setGoodsInfo(goodsList);

            // 查询地址
            Address address = orderService.getAddressByKey(order.getAddressid());
            order.setAddress(address);

            orderList.set(i, order);
        }

        // 显示几个页号
        PageInfo page = new PageInfo(orderList, 5);
        model.addAttribute("pageInfo", page);

        return "adminAllOrder";
    }
    /**
     * 发货操作处理方法
     * - 请求路径：/admin/order/sendGoods
     * - 参数：订单ID(orderid)，HTTP会话对象(session)
     * - 处理流程：
     *    1. 获取管理员信息，如果未登录则重定向至登录页面。
     *    2. 创建Order对象，设置订单ID和发货状态为true。
     *    3. 调用OrderService的updateOrderByKey方法更新订单信息，表示已发货。
     *    4. 发送信息给用户，例如：管理员已经发货了（注释中提到的emailService.sendEmailToUser()）。
     *    5. 返回重定向至待发货订单页面的视图名称。
     */
    @RequestMapping("/sendGoods")
    public String sendGoods(Integer orderid, HttpSession session) {
        Admin admin = (Admin) session.getAttribute("admin");
        if (admin == null) {
            return "redirect:/admin/login";
        }

        Order order = new Order();
        order.setOrderid(orderid);
        order.setIssend(true);
        orderService.updateOrderByKey(order);

        // 发送信息给用户，例如：管理员已经发货了(emailService.sendEmailToUser())

        return "redirect:/admin/order/send";
    }

    /**
     * 待收货订单展示方法
     * - 请求路径：/admin/order/receiver
     * - 参数：当前页码(pn)，Spring MVC的Model对象(model)，HTTP会话对象(session)
     * - 处理流程：
     *    1. 获取管理员信息，如果未登录则重定向至登录页面。
     *    2. 使用PageHelper进行分页，一页显示2个订单。
     *    3. 查询已发货但未收货订单列表，将结果存入model中。
     *    4. 遍历订单列表，查询每个订单中的商品信息和地址信息，设置到订单对象中。
     *    5. 返回收货订单页面的视图名称。
     */
    @RequestMapping("/receiver")
    public String receiveOrder(@RequestParam(value = "page", defaultValue = "1") Integer pn, Model model, HttpSession session) {
        Admin admin = (Admin) session.getAttribute("admin");
        if (admin == null) {
            return "redirect:/admin/login";
        }

        // 一页显示几个数据
        PageHelper.startPage(pn, 2);

        // 查询未收货订单
        OrderExample orderExample = new OrderExample();
        orderExample.or().andIssendEqualTo(true).andIsreceiveEqualTo(false);
        List<Order> orderList = orderService.selectOrderByExample(orderExample);
        model.addAttribute("sendOrder", orderList);

        // 查询该订单中的商品
        for (int i = 0; i < orderList.size(); i++) {
            // 获取订单项中的goodsid
            Order order = orderList.get(i);
            OrderItemExample orderItemExample = new OrderItemExample();
            orderItemExample.or().andOrderidEqualTo(order.getOrderid());
            List<OrderItem> orderItemList = orderService.getOrderItemByExample(orderItemExample);
            List<Integer> goodsIdList = new ArrayList<>();

            List<Goods> goodsList = new ArrayList<>();
            for (OrderItem orderItem : orderItemList) {
                Goods goods = goodsService.selectById(orderItem.getGoodsid());
                goods.setNum(orderItem.getNum());
                goodsList.add(goods);
            }
            order.setGoodsInfo(goodsList);

            // 查询地址
            Address address = orderService.getAddressByKey(order.getAddressid());
            order.setAddress(address);

            orderList.set(i, order);
        }

        // 显示几个页号
        PageInfo page = new PageInfo(orderList, 5);
        model.addAttribute("pageInfo", page);

        return "adminOrderReceive";
    }

    /**
     * 已完成订单展示方法
     * - 请求路径：/admin/order/complete
     * - 参数：当前页码(pn)，Spring MVC的Model对象(model)，HTTP会话对象(session)
     * - 处理流程：
     *    1. 获取管理员信息，如果未登录则重定向至登录页面。
     *    2. 使用PageHelper进行分页，一页显示2个订单。
     *    3. 查询已完成订单列表，将结果存入model中。
     *    4. 遍历订单列表，查询每个订单中的商品信息和地址信息，设置到订单对象中。
     *    5. 返回已完成订单页面的视图名称。
     */
    @RequestMapping("/complete")
    public String completeOrder(@RequestParam(value = "page", defaultValue = "1") Integer pn, Model model, HttpSession session) {
        Admin admin = (Admin) session.getAttribute("admin");
        if (admin == null) {
            return "redirect:/admin/login";
        }

        // 一页显示几个数据
        PageHelper.startPage(pn, 2);

        // 查询已完成订单
        OrderExample orderExample = new OrderExample();
        orderExample.or().andIssendEqualTo(true).andIsreceiveEqualTo(true).andIscompleteEqualTo(true);
        List<Order> orderList = orderService.selectOrderByExample(orderExample);
        model.addAttribute("sendOrder", orderList);

        // 查询该订单中的商品
        for (int i = 0; i < orderList.size(); i++) {
            // 获取订单项中的goodsid
            Order order = orderList.get(i);
            OrderItemExample orderItemExample = new OrderItemExample();
            orderItemExample.or().andOrderidEqualTo(order.getOrderid());
            List<OrderItem> orderItemList = orderService.getOrderItemByExample(orderItemExample);
            List<Integer> goodsIdList = new ArrayList<>();

            List<Goods> goodsList = new ArrayList<>();
            for (OrderItem orderItem : orderItemList) {
                Goods goods = goodsService.selectById(orderItem.getGoodsid());
                goods.setNum(orderItem.getNum());
                goodsList.add(goods);
            }

            // 查询地址
            Address address = orderService.getAddressByKey(order.getAddressid());
            order.setAddress(address);

            orderList.set(i, order);
        }

        // 显示几个页号
        PageInfo page = new PageInfo(orderList, 5);
        model.addAttribute("pageInfo", page);

        return "adminOrderComplete";
    }

}
