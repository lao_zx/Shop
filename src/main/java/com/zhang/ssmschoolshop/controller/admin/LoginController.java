package com.zhang.ssmschoolshop.controller.admin;


import com.zhang.ssmschoolshop.entity.Admin;
import com.zhang.ssmschoolshop.service.AdminService;
import com.zhang.ssmschoolshop.util.Md5Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * LoginController是处理后台管理员登录相关请求的控制器类。
 */
@Controller
@RequestMapping("/admin")
public class LoginController {

    @Autowired
    private AdminService adminService;

    /**
     * 转发到管理员登录页面的请求
     *
     * @return 管理员登录页面视图
     */
    @RequestMapping("/login")
    public String adminLogin() {
        return "adminLogin";
    }

    /**
     * 处理管理员登录验证的请求
     *
     * @param admin  包含管理员用户名和密码的Admin对象
     * @param model  Model对象，用于在视图中传递数据
     * @param request HttpServletRequest对象，用于获取HttpSession
     * @return 若登录验证成功，重定向到管理员用户展示页面；否则，返回登录页面并显示错误信息
     */
    @RequestMapping("/confirmLogin")
    public String confirmLogin(Admin admin, Model model, HttpServletRequest request) {
        // 对密码进行MD5加密
        admin.setPassword(Md5Util.MD5Encode(admin.getPassword(), "utf-8"));

        // 通过用户名和加密后的密码查询数据库
        Admin selectAdmin = adminService.selectByName(admin);
        if (selectAdmin == null) {
            // 登录失败，设置错误信息并返回登录页面
            model.addAttribute("errorMsg", "用户名或密码错误");
            return "adminLogin";
        } else {
            // 登录成功，将管理员信息存入Session，并重定向到管理员用户展示页面
            HttpSession session = request.getSession();
            session.setAttribute("admin", selectAdmin);
            return "redirect:/admin/user/show";
        }
    }

    /**
     * 处理管理员注销的请求
     *
     * @param request HttpServletRequest对象，用于获取HttpSession
     * @return 重定向到管理员登录页面
     */
    @RequestMapping("/logout")
    public String adminLogout(HttpServletRequest request) {
        // 从Session中移除管理员信息，实现注销操作
        HttpSession session = request.getSession();
        session.removeAttribute("admin");
        return "redirect:/admin/login";
    }
}

