package com.zhang.ssmschoolshop.controller.admin;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import com.zhang.ssmschoolshop.entity.User;
import com.zhang.ssmschoolshop.entity.UserExample;
import com.zhang.ssmschoolshop.service.UserService;
import com.zhang.ssmschoolshop.util.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;


/**
 * UserController是处理后台用户管理相关请求的控制器类。
 */
@Controller
@RequestMapping("/admin/user")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 处理显示所有用户信息的请求，返回JSON格式的用户数据
     *
     * @param pn      当前页码，默认为1
     * @param model   Model对象，用于在视图中传递数据
     * @return Msg对象，包含查询结果和分页信息
     */
    @RequestMapping("/showjson")
    @ResponseBody
    public Msg getAllGoods(@RequestParam(value = "page", defaultValue = "1") Integer pn, HttpServletResponse response, Model model) {
        // 一页显示几个数据
        PageHelper.startPage(pn, 10);

        // 查询所有用户信息
        List<User> userList = userService.selectByExample(new UserExample());

        // 显示几个页号
        PageInfo page = new PageInfo(userList, 5);

        return Msg.success("查询成功!").add("pageInfo", page);
    }

    /**
     * 转发到用户管理页面的请求
     *
     * @return 用户管理页面视图
     */
    @RequestMapping("/show")
    public String userManage() {
        return "userManage";
    }

    /**
     * 处理删除用户的请求
     *
     * @param userid  待删除用户的ID
     * @return Msg对象，包含删除成功的提示信息
     */
    @RequestMapping(value = "/delete/{userid}", method = RequestMethod.DELETE)
    @ResponseBody
    public Msg deleteUser(@PathVariable("userid") Integer userid) {
        // 调用userService删除指定ID的用户
        userService.deleteUserById(userid);
        return Msg.success("删除成功!");
    }
}

