/**
 * GoodsController是处理后台商品管理相关请求的控制器类。
 */
package com.zhang.ssmschoolshop.controller.admin;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zhang.ssmschoolshop.entity.*;
import com.zhang.ssmschoolshop.service.CateService;
import com.zhang.ssmschoolshop.service.GoodsService;
import com.zhang.ssmschoolshop.util.ImageUtil;
import com.zhang.ssmschoolshop.util.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/admin/goods")
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    @Autowired
    private CateService cateService;

    /**
     * 以JSON格式返回所有商品信息
     * @param pn 当前页码
     * @param response HttpServletResponse
     * @param model Model
     * @param session HttpSession
     * @return 包含商品信息的Msg对象
     */
    @RequestMapping("/showjson")
    @ResponseBody
    public Msg getAllGoods(@RequestParam(value = "page", defaultValue = "1") Integer pn,
                           HttpServletResponse response, Model model, HttpSession session) {
        Admin admin = (Admin) session.getAttribute("admin");
        if (admin == null) {
            return Msg.fail("请先登录");
        }
        //一页显示几个数据
        PageHelper.startPage(pn, 10);

        List<Goods> goodsList = goodsService.selectByExample(new GoodsExample());

        //显示几个页号
        PageInfo page = new PageInfo(goodsList, 5);

        model.addAttribute("pageInfo", page);

        return Msg.success("查询成功!").add("pageInfo", page);
    }

    /**
     * 转发到商品管理页面
     * @param pn 当前页码
     * @param response HttpServletResponse
     * @param model Model
     * @param session HttpSession
     * @return 商品管理页面视图
     * @throws IOException IOException
     */
    @RequestMapping("/show")
    public String goodsManage(@RequestParam(value = "page", defaultValue = "1") Integer pn,
                              HttpServletResponse response, Model model, HttpSession session) throws IOException {
        Admin admin = (Admin) session.getAttribute("admin");
        if (admin == null) {
            return "redirect:/admin/login";
        }

        List<Category> categoryList = cateService.selectByExample(new CategoryExample());
        model.addAttribute("categoryList", categoryList);

        return "adminAllGoods";
    }

    /**
     * 转发到商品添加页面
     * @param msg 添加商品的成功消息
     * @param model Model
     * @param session HttpSession
     * @return 商品添加页面视图
     */
    @RequestMapping("/add")
    public String showAdd(@ModelAttribute("succeseMsg") String msg, Model model, HttpSession session) {
        Admin admin = (Admin) session.getAttribute("admin");
        if (admin == null) {
            return "redirect:/admin/login";
        }

        if (!msg.equals("")) {
            model.addAttribute("msg", msg);
        }

        List<Category> categoryList = cateService.selectByExample(new CategoryExample());
        model.addAttribute("categoryList", categoryList);

        return "addGoods";
    }

    /**
     * 处理商品更新请求
     * @param goods 更新后的商品信息
     * @param session HttpSession
     * @return 包含更新结果信息的Msg对象
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public Msg updateGoods(Goods goods, HttpSession session) {
        Admin admin = (Admin) session.getAttribute("admin");
        if (admin == null) {
            return Msg.fail("请先登录");
        }
        goodsService.updateGoodsById(goods);
        return Msg.success("更新成功!");
    }

    /**
     * 处理商品删除请求
     * @param goodsid 要删除的商品ID
     * @return 包含删除结果信息的Msg对象
     */
    @RequestMapping(value = "/delete/{goodsid}", method = RequestMethod.DELETE)
    @ResponseBody
    public Msg deleteGoods(@PathVariable("goodsid") Integer goodsid) {
        goodsService.deleteGoodsById(goodsid);
        return Msg.success("删除成功!");
    }

    /**
     * 处理商品添加请求
     * @param goods 新添加的商品信息
     * @param fileToUpload 上传的图片文件
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @param redirectAttributes RedirectAttributes
     * @return 重定向到商品添加页面
     * @throws IOException IOException
     */
    @RequestMapping("/addGoodsSuccess")
    public String addGoods(Goods goods,
                           @RequestParam MultipartFile[] fileToUpload,
                           HttpServletRequest request,
                           HttpServletResponse response,
                           RedirectAttributes redirectAttributes) throws IOException {
        goods.setUptime(new Date());
        goods.setActivityid(1);
        goodsService.addGoods(goods);
        for (MultipartFile multipartFile : fileToUpload) {
            String fileName = goods.getGoodsname() + multipartFile.getOriginalFilename();
            if (multipartFile != null) {
                String imagePath = ImageUtil.imagePath(multipartFile, fileName);
                System.out.println("最后存入数据的图片名字为:" + imagePath);
                //把图片路径存入数据库中
                goodsService.addImagePath(new ImagePath(null, goods.getGoodsid(), imagePath));
            }
        }

        redirectAttributes.addFlashAttribute("succeseMsg", "商品添加成功!");

        return "redirect:/admin/goods/add";
    }

    /**
     * 转发到商品分类添加页面
     * @param msg 添加分类的成功消息
     * @param model Model
     * @param session HttpSession
     * @return 商品分类添加页面视图
     */
    @RequestMapping("/addCategory")
    public String addcategory(@ModelAttribute("succeseMsg") String msg, Model model, HttpSession session) {
        Admin admin = (Admin) session.getAttribute("admin");
        if (admin == null) {
            return "redirect:/admin/login";
        }
        CategoryExample categoryExample = new CategoryExample();
        categoryExample.or();
        List<Category> categoryList;
        categoryList = cateService.selectByExample(categoryExample);
        model.addAttribute("categoryList", categoryList);
        if (!msg.equals("")) {
            model.addAttribute("msg", msg);
        }
        return "addCategory";
    }

    /**
     * 处理商品分类添加请求
     * @param category 新添加的商品分类信息
     * @param addCategoryResult Model
     * @param redirectAttributes RedirectAttributes
     * @return 重定向到商品分类添加页面
     */
    @RequestMapping("/addCategoryResult")
    public String addCategoryResult(Category category, Model addCategoryResult, RedirectAttributes redirectAttributes) {
        List<Category> categoryList = new ArrayList<>();
        CategoryExample categoryExample = new CategoryExample();
        categoryExample.or().andCatenameEqualTo(category.getCatename());
        categoryList = cateService.selectByExample(categoryExample);
        if (!categoryList.isEmpty()) {
            redirectAttributes.addAttribute("succeseMsg", "分类已存在");
            return "redirect:/admin/goods/addCategory";
        } else {
            cateService.insertSelective(category);
            redirectAttributes.addFlashAttribute("succeseMsg", "分类添加成功!");
            return "redirect:/admin/goods/addCategory";
        }
    }

    /**
     * 处理商品分类更新请求
     * @param category 更新后的商品分类信息
     * @return 包含更新结果信息的Msg对象
     */
    @RequestMapping("/saveCate")
    @ResponseBody
    public Msg saveCate(Category category) {
        CategoryExample categoryExample = new CategoryExample();
        categoryExample.or().andCatenameEqualTo(category.getCatename());
        List<Category> categoryList = cateService.selectByExample(categoryExample);
        if (categoryList.isEmpty()) {
            cateService.updateByPrimaryKeySelective(category);
            return Msg.success("更新成功");
        } else return Msg.success("名字已经存在");
    }

    /**
     * 处理商品分类删除请求
     * @param category 要删除的商品分类信息
     * @return 包含删除结果信息的Msg对象
     */
    @RequestMapping("/deleteCate")
    @ResponseBody
    public Msg deleteCate(Category category) {
        cateService.deleteByPrimaryKey(category.getCateid());
        return Msg.success("删除成功");
    }
}
