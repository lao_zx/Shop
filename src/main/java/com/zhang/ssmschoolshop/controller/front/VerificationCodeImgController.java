package com.zhang.ssmschoolshop.controller.front;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 控制验证码图片页面的显示。
 */
@Controller
public class VerificationCodeImgController {

    /**
     * 处理获取验证码图片页面的请求。
     *
     * @return ModelAndView对象，包含验证码图片页面的视图信息。
     */
    @RequestMapping("/verificationcodeimg")
    public ModelAndView verificationcodeimg(){
        ModelAndView verificationcodeimg=new ModelAndView();
        verificationcodeimg.setViewName("verificationcodeimg");
        return verificationcodeimg;
    }
}
