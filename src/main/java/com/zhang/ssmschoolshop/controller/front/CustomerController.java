package com.zhang.ssmschoolshop.controller.front;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zhang.ssmschoolshop.entity.*;
import com.zhang.ssmschoolshop.service.AddressService;
import com.zhang.ssmschoolshop.service.GoodsService;
import com.zhang.ssmschoolshop.service.OrderService;
import com.zhang.ssmschoolshop.service.UserService;
import com.zhang.ssmschoolshop.util.Md5Util;
import com.zhang.ssmschoolshop.util.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class CustomerController {

    /**
     * 显示登录页面。
     *
     * @return 登录页面视图。
     */
    @RequestMapping("/login")
    public String loginView() {
        return "login";
    }

    @Autowired
    private UserService userService;

    /**
     * 显示注册页面。
     *
     * @return 注册页面视图。
     */
    @RequestMapping("/register")
    public String register() {
        return "register";
    }

    /**
     * 处理用户注册请求，进行用户注册操作。
     *
     * @param user           用户对象。
     * @param registerResult 用于存储注册结果的模型对象。
     * @return 注册成功则重定向到登录页面，否则返回注册页面。
     */
    @RequestMapping("/registerresult")
    public String registerResult(User user, Model registerResult) {
        // 将密码进行MD5加密
        user.setPassword(Md5Util.MD5Encode(user.getPassword(), "utf-8"));

        // 检查用户名是否已被占用
        UserExample userExample = new UserExample();
        userExample.or().andUsernameLike(user.getUsername());
        List<User> userList = userService.selectByExample(userExample);

        if (!userList.isEmpty()) {
            registerResult.addAttribute("errorMsg", "用户名被占用");
            return "register";
        } else {
            // 设置注册时间并插入新用户
            Date regTime = new Date();
            user.setRegtime(regTime);
            userService.insertSelective(user);
            return "redirect:/login";
        }
    }

    /**
     * 处理用户登录请求，进行用户登录验证。
     *
     * @param user          用户对象。
     * @param loginResult   用于存储登录结果的模型对象。
     * @param request       HttpServletRequest 对象。
     * @param confirmlogo   验证码。
     * @return 登录成功则重定向到主页面，否则返回登录页面。
     */
    @RequestMapping("/loginconfirm")
    public String loginConfirm(User user, Model loginResult, HttpServletRequest request, @RequestParam("confirmlogo") String confirmlogo) {
        System.out.println("传进来的用户帐号和密码为:" + user);
        // 进行用户密码MD5加密验证
        user.setPassword(Md5Util.MD5Encode(user.getPassword(), "UTF-8"));
        HttpSession session = request.getSession();
        String verificationCode = (String) session.getAttribute("certCode");
        if (!confirmlogo.equals(verificationCode)) {
            loginResult.addAttribute("errorMsg", "验证码错误");
            return "login";
        }
        List<User> userList = new ArrayList<User>();
        UserExample userExample = new UserExample();
        userExample.or().andUsernameEqualTo(user.getUsername()).andPasswordEqualTo(user.getPassword());
        userList = userService.selectByExample(userExample);
        if (!userList.isEmpty()) {
            session.setAttribute("user", userList.get(0));

            // 判断是否为 admin
            boolean isAdmin = "admin".equals(userList.get(0).getUsername());
            session.setAttribute("isAdmin", isAdmin);

            return "redirect:/main";
        } else {
            loginResult.addAttribute("errorMsg", "用户名与密码不匹配");
            return "login";
        }
    }

    /**
     * 显示用户信息页面。
     *
     * @param userModel 用于存储用户信息的模型对象。
     * @param request   HttpServletRequest 对象。
     * @return 用户信息页面视图。
     */
    @RequestMapping("/information")
    public String information(Model userModel, HttpServletRequest request) {
        HttpSession session = request.getSession();
        User user;
        Integer userId;
        user = (User) session.getAttribute("user");
        if (user == null) {
            return "redirect:/login";
        }
        userId = user.getUserid();
        user = userService.selectByPrimaryKey(userId);
        userModel.addAttribute("user", user);
        return "information";
    }

    /**
     * 处理保存用户信息请求，更新用户信息。
     *
     * @param name      用户名。
     * @param email     电子邮件。
     * @param telephone 电话号码。
     * @param request   HttpServletRequest 对象。
     * @return 包含更新结果的 Msg 对象。
     */
    @RequestMapping("/saveInfo")
    @ResponseBody
    public Msg saveInfo(String name, String email, String telephone, HttpServletRequest request) {
        HttpSession session = request.getSession();
        UserExample userExample = new UserExample();
        User user, updateUser = new User();
        List<User> userList = new ArrayList<>();
        Integer userid;
        user = (User) session.getAttribute("user");
        userid = user.getUserid();
        userExample.or().andUsernameEqualTo(name);
        userList = userService.selectByExample(userExample);
        if (userList.isEmpty()) {
            updateUser.setUserid(userid);
            updateUser.setUsername(name);
            updateUser.setEmail(email);
            updateUser.setTelephone(telephone);
            userService.updateByPrimaryKeySelective(updateUser);
            return Msg.success("更新成功");
        } else {
            return Msg.fail("更新失败");
        }
    }

    @Autowired
    private AddressService addressService;

    /**
     * 显示用户地址页面。
     *
     * @param request      HttpServletRequest 对象。
     * @param addressModel 用于存储地址信息的模型对象。
     * @return 用户地址页面视图。
     */
    @RequestMapping("/info/address")
    public String address(HttpServletRequest request, Model addressModel) {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        if (user == null) {
            return "redirect:/login";
        }
        AddressExample addressExample = new AddressExample();
        addressExample.or().andUseridEqualTo(user.getUserid());
        List<Address> addressList = addressService.getAllAddressByExample(addressExample);
        addressModel.addAttribute("addressList", addressList);
        return "address";
    }

    /**
     * 处理保存地址信息请求，更新地址信息。
     *
     * @param address 地址对象。
     * @return 包含更新结果的 Msg 对象。
     */
    @RequestMapping("/saveAddr")
    @ResponseBody
    public Msg saveAddr(Address address) {
        addressService.updateByPrimaryKeySelective(address);
        return Msg.success("修改成功");
    }

    /**
     * 处理删除地址信息请求，删除地址。
     *
     * @param address 地址对象。
     * @return 包含删除结果的 Msg 对象。
     */
    @RequestMapping("/deleteAddr")
    @ResponseBody
    public Msg deleteAddr(Address address) {
        addressService.deleteByPrimaryKey(address.getAddressid());
        return Msg.success("删除成功");
    }

    /**
     * 处理添加新地址请求，添加新地址信息。
     *
     * @param address 新地址对象。
     * @param request HttpServletRequest 对象。
     * @return 包含添加结果的 Msg 对象。
     */
    @RequestMapping("/insertAddr")
    @ResponseBody
    public Msg insertAddr(Address address, HttpServletRequest request) {
        HttpSession session = request.getSession();
        User user = new User();
        user = (User) session.getAttribute("user");
        address.setUserid(user.getUserid());
        addressService.insertSelective(address);
        return Msg.success("添加成功");
    }


    @Autowired
    private OrderService orderService;

    @Autowired
    private GoodsService goodsService;

    /**
     * 显示用户订单列表页面。
     *
     * @param request    HttpServletRequest 对象。
     * @param orderModel 用于存储订单信息的模型对象。
     * @return 用户订单列表页面视图。
     */
    @RequestMapping("/info/list")
    public String list(HttpServletRequest request, Model orderModel) {

        HttpSession session = request.getSession();
        User user;
        user = (User) session.getAttribute("user");

        if (user == null) {
            return "redirect:/login";
        }

        OrderExample orderExample = new OrderExample();
        orderExample.or().andUseridEqualTo(user.getUserid());

        List<Order> orderList = orderService.selectOrderByExample(orderExample);
        orderModel.addAttribute("orderList", orderList);
        Order order;
        OrderItem orderItem;
        List<OrderItem> orderItemList = new ArrayList<>();
        Goods goods;
        Address address;
        for (Integer i = 0; i < orderList.size(); i++) {
            order = orderList.get(i);
            OrderItemExample orderItemExample = new OrderItemExample();
            orderItemExample.or().andOrderidEqualTo(order.getOrderid());
            orderItemList = orderService.getOrderItemByExample(orderItemExample);
            List<Goods> goodsList = new ArrayList<>();
            List<Integer> goodsIdList = new ArrayList<>();
            for (Integer j = 0; j < orderItemList.size(); j++) {
                orderItem = orderItemList.get(j);
                goodsIdList.add(orderItem.getGoodsid());
            }
            GoodsExample goodsExample = new GoodsExample();
            goodsExample.or().andGoodsidIn(goodsIdList);
            goodsList = goodsService.selectByExample(goodsExample);
            order.setGoodsInfo(goodsList);
            address = addressService.selectByPrimaryKey(order.getAddressid());
            order.setAddress(address);
            orderList.set(i, order);
        }
        orderModel.addAttribute("orderList", orderList);

        return "list";
    }

    /**
     * 处理删除订单请求，删除订单。
     *
     * @param order 包含订单信息的 Order 对象。
     * @return 包含删除结果的 Msg 对象。
     */
    @RequestMapping("/deleteList")
    @ResponseBody
    public Msg deleteList(Order order) {
        orderService.deleteById(order.getOrderid());
        return Msg.success("删除成功");
    }

    /**
     * 显示用户收藏商品页面。
     *
     * @param pn      当前页码。
     * @param request HttpServletRequest 对象。
     * @param model   用于存储商品信息的模型对象。
     * @return 用户收藏商品页面视图。
     */
    @RequestMapping("/info/favorite")
    public String showFavorite(@RequestParam(value = "page", defaultValue = "1") Integer pn, HttpServletRequest request, Model model) {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        if (user == null) {
            return "redirect:/login";
        }

        //一页显示几个数据
        PageHelper.startPage(pn, 16);

        FavoriteExample favoriteExample = new FavoriteExample();
        favoriteExample.or().andUseridEqualTo(user.getUserid());
        List<Favorite> favoriteList = goodsService.selectFavByExample(favoriteExample);

        List<Integer> goodsIdList = new ArrayList<Integer>();
        for (Favorite tmp : favoriteList) {
            goodsIdList.add(tmp.getGoodsid());
        }

        GoodsExample goodsExample = new GoodsExample();
        List<Goods> goodsList = new ArrayList<>();
        if (!goodsIdList.isEmpty()) {
            goodsExample.or().andGoodsidIn(goodsIdList);
            goodsList = goodsService.selectByExample(goodsExample);
        }

        //获取图片地址
        for (int i = 0; i < goodsList.size(); i++) {
            Goods goods = goodsList.get(i);

            List<ImagePath> imagePathList = goodsService.findImagePath(goods.getGoodsid());

            goods.setImagePaths(imagePathList);

            //判断是否收藏
            goods.setFav(true);

            goodsList.set(i, goods);
        }

        //显示几个页号
        PageInfo page = new PageInfo(goodsList, 5);
        model.addAttribute("pageInfo", page);

        return "favorite";
    }

    /**
     * 处理保存密码请求，更新用户密码。
     *
     * @param Psw     新密码。
     * @param request HttpServletRequest 对象。
     * @return 包含更新结果的 Msg 对象。
     */
    @RequestMapping("/savePsw")
    @ResponseBody
    public Msg savePsw(String Psw, HttpServletRequest request) {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        user.setPassword(Md5Util.MD5Encode(Psw, "UTF-8"));
        userService.updateByPrimaryKeySelective(user);
        return Msg.success("修改密码成功");
    }

    /**
     * 处理完成订单请求，更新订单状态。
     *
     * @param orderid 订单ID。
     * @return 包含更新结果的 Msg 对象。
     */
    @RequestMapping("/finishList")
    @ResponseBody
    public Msg finishiList(Integer orderid) {
        Order order = orderService.selectByPrimaryKey(orderid);
        order.setIsreceive(true);
        order.setIscomplete(true);
        orderService.updateOrderByKey(order);
        return Msg.success("完成订单成功");
    }

    /**
     * 处理用户注销请求，清除 session 中的用户信息。
     *
     * @param request HttpServletRequest 对象。
     * @return 重定向到登录页面。
     */
    @RequestMapping("/logout")
    public String logout(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.removeAttribute("user");
        return "redirect:/login";
    }

}
