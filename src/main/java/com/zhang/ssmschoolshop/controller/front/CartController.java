package com.zhang.ssmschoolshop.controller.front;


import com.zhang.ssmschoolshop.entity.*;
import com.zhang.ssmschoolshop.service.GoodsService;
import com.zhang.ssmschoolshop.service.ShopCartService;
import com.zhang.ssmschoolshop.util.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * CartController是处理前台购物车相关请求的控制器类。
 */
@Controller
public class CartController {

    @Autowired
    private ShopCartService shopCartService;

    @Autowired
    private GoodsService goodsService;

    /**
     * 处理添加购物车请求
     *
     * @param shopCart 购物车对象，包含要添加的商品信息
     * @param request  HttpServletRequest对象，用于获取Session
     * @return 重定向到购物车页面
     */
    @RequestMapping("/addCart")
    public String addCart(ShopCart shopCart, HttpServletRequest request) {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        if (user == null) {
            return "redirect:/login";
        }

        // 判断是否已经加入购物车
        ShopCart shopCart1 = shopCartService.selectCartByKey(new ShopCartKey(user.getUserid(), shopCart.getGoodsid()));
        if (shopCart1 != null) {
            return "redirect:/showcart";
        }

        // 设置用户ID和加入时间
        shopCart.setUserid(user.getUserid());
        shopCart.setCatedate(new Date());

        // 调用shopCartService添加购物车信息
        shopCartService.addShopCart(shopCart);

        // 重定向到购物车页面
        return "redirect:/showcart";
    }

    /**
     * 转发到购物车页面的请求
     *
     * @param session HttpSession对象，用于获取用户信息
     * @return 购物车页面视图
     */
    @RequestMapping("/showcart")
    public String showCart(HttpSession session) {
        User user = (User) session.getAttribute("user");
        if (user == null) {
            return "redirect:/login";
        }
        return "shopcart";
    }

    /**
     * 处理获取购物车信息的请求，返回JSON格式的购物车数据
     *
     * @param session HttpSession对象，用于获取用户信息
     * @return Msg对象，包含查询结果和购物车信息
     */
    @RequestMapping("/cartjson")
    @ResponseBody
    public Msg getCart(HttpSession session) {
        User user = (User) session.getAttribute("user");
        if (user == null) {
            return Msg.fail("请先登录");
        }

        // 获取当前用户的购物车信息
        ShopCartExample shopCartExample = new ShopCartExample();
        shopCartExample.or().andUseridEqualTo(user.getUserid());
        List<ShopCart> shopCart = shopCartService.selectByExample(shopCartExample);

        // 获取购物车中的商品信息
        List<Goods> goodsAndImage = new ArrayList<>();
        for (ShopCart cart : shopCart) {
            Goods goods = goodsService.selectById(cart.getGoodsid());
            List<ImagePath> imagePathList = goodsService.findImagePath(goods.getGoodsid());
            goods.setImagePaths(imagePathList);
            goods.setNum(cart.getGoodsnum());
            goodsAndImage.add(goods);
        }

        return Msg.success("查询成功").add("shopcart", goodsAndImage);
    }

    /**
     * 处理删除购物车商品的请求
     *
     * @param goodsid 商品ID
     * @param session HttpSession对象，用于获取用户信息
     * @return Msg对象，包含删除成功的提示信息
     */
    @RequestMapping(value = "/deleteCart/{goodsid}", method = RequestMethod.DELETE)
    @ResponseBody
    public Msg deleteCart(@PathVariable("goodsid") Integer goodsid, HttpSession session) {
        User user = (User) session.getAttribute("user");
        if (user == null) {
            return Msg.fail("请先登录");
        }

        // 调用shopCartService删除购物车中指定ID的商品
        shopCartService.deleteByKey(new ShopCartKey(user.getUserid(), goodsid));
        return Msg.success("删除成功");
    }

    /**
     * 处理更新购物车商品数量的请求
     *
     * @param goodsid 商品ID
     * @param num     商品数量
     * @param session HttpSession对象，用于获取用户信息
     * @return Msg对象，包含更新成功的提示信息
     */
    @RequestMapping("/update")
    @ResponseBody
    public Msg updateCart(Integer goodsid, Integer num, HttpSession session) {
        User user = (User) session.getAttribute("user");
        if (user == null) {
            return Msg.fail("请先登录");
        }

        // 构造ShopCart对象，设置用户ID、商品ID和商品数量
        ShopCart shopCart = new ShopCart();
        shopCart.setUserid(user.getUserid());
        shopCart.setGoodsid(goodsid);
        shopCart.setGoodsnum(num);

        // 调用shopCartService更新购物车信息
        shopCartService.updateCartByKey(shopCart);
        return Msg.success("更新购物车成功");
    }
}
