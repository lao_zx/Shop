package com.zhang.ssmschoolshop.service;

import com.zhang.ssmschoolshop.entity.Category;
import com.zhang.ssmschoolshop.entity.CategoryExample;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 商品分类服务接口
 */
@Service("CateService")
public interface CateService {

    /**
     * 根据条件查询商品分类列表
     *
     * @param example 包含查询条件的商品分类示例对象
     * @return 查询到的商品分类列表
     */
    List<Category> selectByExample(CategoryExample example);

    /**
     * 插入商品分类信息（可选择性插入）
     *
     * @param category 包含商品分类信息的对象
     */
    void insertSelective(Category category);

    /**
     * 根据条件查询一定数量的商品分类列表
     *
     * @param digCategoryExample 包含查询条件的商品分类示例对象
     * @return 查询到的商品分类列表
     */
    List<Category> selectByExampleLimit(CategoryExample digCategoryExample);

    /**
     * 根据商品分类ID查询商品分类信息
     *
     * @param category 商品分类ID
     * @return 查询到的商品分类信息
     */
    Category selectById(Integer category);

    /**
     * 根据主键选择性更新商品分类信息
     *
     * @param category 包含商品分类信息的对象
     */
    void updateByPrimaryKeySelective(Category category);

    /**
     * 根据商品分类ID删除商品分类信息
     *
     * @param cateid 商品分类ID
     */
    void deleteByPrimaryKey(Integer cateid);
}
