package com.zhang.ssmschoolshop.service;

/**
 * 邮件服务接口
 */
public interface EmailService {

    /**
     * 发送邮件给管理员，通知用户购买成功
     */
    void sendEmailToAdmin();

    /**
     * 发送邮件给用户，通知管理员已经发货
     */
    void sendEmailToUser();
}
