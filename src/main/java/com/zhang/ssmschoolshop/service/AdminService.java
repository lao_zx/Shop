package com.zhang.ssmschoolshop.service;

import com.zhang.ssmschoolshop.entity.Admin;

/**
 * 管理员服务接口
 */
public interface AdminService {

    /**
     * 根据管理员对象查询管理员信息
     *
     * @param admin 包含查询信息的管理员对象
     * @return 查询到的管理员信息
     */
    Admin selectByName(Admin admin);
}
