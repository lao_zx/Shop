package com.zhang.ssmschoolshop.service;

import com.zhang.ssmschoolshop.entity.Comment;
import com.zhang.ssmschoolshop.entity.CommentExample;

import java.util.List;

/**
 * 评论服务接口
 */
public interface CommentService {

    /**
     * 插入评论信息（可选择性插入）
     *
     * @param comment 包含评论信息的对象
     */
    void insertSelective(Comment comment);

    /**
     * 根据条件查询评论列表
     *
     * @param commentExample 包含查询条件的评论示例对象
     * @return 查询到的评论列表
     */
    List<Comment> selectByExample(CommentExample commentExample);
}
