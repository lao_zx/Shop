package com.zhang.ssmschoolshop.service;

import com.zhang.ssmschoolshop.entity.Activity;
import com.zhang.ssmschoolshop.entity.ActivityExample;

import java.util.List;

/**
 * 活动服务接口
 */
public interface ActivityService {

    /**
     * 获取所有活动信息
     *
     * @param activityExample 活动查询条件
     * @return 包含活动信息的列表
     */
    List<Activity> getAllActivity(ActivityExample activityExample);

    /**
     * 插入活动信息（可选择性插入）
     *
     * @param activity 活动信息
     */
    void insertActivitySelective(Activity activity);

    /**
     * 根据活动ID查询活动信息
     *
     * @param activityid 活动ID
     * @return 活动信息
     */
    Activity selectByKey(Integer activityid);

    /**
     * 根据活动ID删除活动
     *
     * @param activityid 活动ID
     */
    void deleteByActivityId(Integer activityid);

    //    void updateGoodsActSelective(Goods goods);
}
