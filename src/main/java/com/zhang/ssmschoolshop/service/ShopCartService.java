package com.zhang.ssmschoolshop.service;

import com.zhang.ssmschoolshop.entity.ShopCart;
import com.zhang.ssmschoolshop.entity.ShopCartExample;
import com.zhang.ssmschoolshop.entity.ShopCartKey;

import java.util.List;

/**
 * 购物车服务接口
 */
public interface ShopCartService {

    /**
     * 添加购物车
     *
     * @param shopCart 购物车对象
     */
    void addShopCart(ShopCart shopCart);

    /**
     * 根据条件查询购物车列表
     *
     * @param shopCartExample 购物车查询条件
     * @return 购物车列表
     */
    List<ShopCart> selectByExample(ShopCartExample shopCartExample);

    /**
     * 根据购物车主键删除购物车
     *
     * @param shopCartKey 购物车主键
     */
    void deleteByKey(ShopCartKey shopCartKey);

    /**
     * 根据购物车主键更新购物车信息
     *
     * @param shopCart 购物车对象
     */
    void updateCartByKey(ShopCart shopCart);

    /**
     * 根据购物车主键查询购物车信息
     *
     * @param shopCartKey 购物车主键
     * @return 购物车对象
     */
    ShopCart selectCartByKey(ShopCartKey shopCartKey);
}
