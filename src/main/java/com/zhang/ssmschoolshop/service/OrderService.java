package com.zhang.ssmschoolshop.service;

import com.zhang.ssmschoolshop.entity.*;

import java.util.List;

/**
 * 订单服务接口
 */
public interface OrderService {

    /**
     * 插入订单
     *
     * @param order 订单对象
     */
    void insertOrder(Order order);

    /**
     * 根据订单ID删除订单
     *
     * @param orderid 订单ID
     */
    void deleteById(Integer orderid);

    /**
     * 根据条件查询订单列表
     *
     * @param orderExample 订单查询条件
     * @return 订单列表
     */
    List<Order> selectOrderByExample(OrderExample orderExample);

    /**
     * 根据条件查询订单项列表
     *
     * @param orderItemExample 订单项查询条件
     * @return 订单项列表
     */
    List<OrderItem> getOrderItemByExample(OrderItemExample orderItemExample);

    /**
     * 根据地址ID获取地址信息
     *
     * @param addressid 地址ID
     * @return 地址信息
     */
    Address getAddressByKey(Integer addressid);

    /**
     * 根据订单ID更新订单信息
     *
     * @param order 订单对象
     */
    void updateOrderByKey(Order order);

    /**
     * 根据订单ID查询订单信息
     *
     * @param orderid 订单ID
     * @return 订单对象
     */
    Order selectByPrimaryKey(Integer orderid);

    /**
     * 插入订单项
     *
     * @param orderItem 订单项对象
     */
    void insertOrderItem(OrderItem orderItem);
}
