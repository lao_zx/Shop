package com.zhang.ssmschoolshop.service;

import com.zhang.ssmschoolshop.entity.Address;
import com.zhang.ssmschoolshop.entity.AddressExample;

import java.util.List;

/**
 * 地址服务接口
 */
public interface AddressService {

    /**
     * 获取所有地址信息
     *
     * @param addressExample 地址查询条件
     * @return 包含地址信息的列表
     */
    List<Address> getAllAddressByExample(AddressExample addressExample);

    /**
     * 根据主键选择性更新地址信息
     *
     * @param address 包含更新信息的地址对象
     */
    void updateByPrimaryKeySelective(Address address);

    /**
     * 根据地址ID删除地址
     *
     * @param addressid 地址ID
     */
    void deleteByPrimaryKey(Integer addressid);

    /**
     * 插入地址信息
     *
     * @param address 包含插入信息的地址对象
     */
    void insert(Address address);

    /**
     * 插入地址信息（可选择性插入）
     *
     * @param address 包含插入信息的地址对象
     */
    void insertSelective(Address address);

    /**
     * 根据地址ID查询地址信息
     *
     * @param addressid 地址ID
     * @return 地址信息
     */
    Address selectByPrimaryKey(Integer addressid);
}
