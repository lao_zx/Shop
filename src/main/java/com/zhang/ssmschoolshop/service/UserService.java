package com.zhang.ssmschoolshop.service;

import com.zhang.ssmschoolshop.entity.User;
import com.zhang.ssmschoolshop.entity.UserExample;

import java.util.List;

/**
 * 用户服务接口
 */
public interface UserService {

    /**
     * 根据用户ID查询用户信息
     *
     * @param userId 用户ID
     * @return 用户对象
     */
    User selectByPrimaryKey(int userId);

    /**
     * 根据条件查询用户列表
     *
     * @param userExample 用户查询条件
     * @return 用户列表
     */
    List<User> selectByExample(UserExample userExample);

    /**
     * 插入用户信息（可选择性插入）
     *
     * @param user 用户对象
     */
    void insertSelective(User user);

    /**
     * 根据用户ID删除用户
     *
     * @param userid 用户ID
     */
    void deleteUserById(Integer userid);

    /**
     * 根据用户ID更新用户信息（可选择性更新）
     *
     * @param user 用户对象
     */
    void updateByPrimaryKeySelective(User user);
}
