package com.zhang.ssmschoolshop.service;

import com.zhang.ssmschoolshop.entity.*;

import java.util.List;

/**
 * 商品服务接口
 */
public interface GoodsService {

    /**
     * 添加商品
     *
     * @param goods 商品对象
     * @return 商品ID
     */
    Integer addGoods(Goods goods);

    /**
     * 添加商品图片路径
     *
     * @param imagePath 商品图片路径对象
     */
    void addImagePath(ImagePath imagePath);

    /**
     * 根据条件查询商品列表
     *
     * @param example 查询条件
     * @return 商品列表
     */
    List<Goods> selectByExample(GoodsExample example);

    /**
     * 根据商品ID删除商品
     *
     * @param goodsid 商品ID
     */
    void deleteGoodsById(Integer goodsid);

    /**
     * 根据商品ID更新商品信息
     *
     * @param goods 商品对象
     */
    void updateGoodsById(Goods goods);

    /**
     * 根据商品ID查找商品图片路径列表
     *
     * @param goodsid 商品ID
     * @return 商品图片路径列表
     */
    List<ImagePath> findImagePath(Integer goodsid);

    /**
     * 根据商品ID查询商品信息
     *
     * @param goodsid 商品ID
     * @return 商品对象
     */
    Goods selectById(Integer goodsid);

    /**
     * 根据条件查询商品列表，带限制条件
     *
     * @param digGoodsExample 查询条件
     * @return 商品列表
     */
    List<Goods> selectByExampleLimit(GoodsExample digGoodsExample);

    /**
     * 添加收藏
     *
     * @param favorite 收藏对象
     */
    void addFavorite(Favorite favorite);

    /**
     * 根据收藏主键查询收藏信息
     *
     * @param favoriteKey 收藏主键
     * @return 收藏对象
     */
    Favorite selectFavByKey(FavoriteKey favoriteKey);

    /**
     * 根据收藏主键删除收藏信息
     *
     * @param favoriteKey 收藏主键
     */
    void deleteFavByKey(FavoriteKey favoriteKey);

    /**
     * 根据条件查询收藏列表
     *
     * @param favoriteExample 查询条件
     * @return 收藏列表
     */
    List<Favorite> selectFavByExample(FavoriteExample favoriteExample);
}
