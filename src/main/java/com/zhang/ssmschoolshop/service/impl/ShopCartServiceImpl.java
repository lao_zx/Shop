package com.zhang.ssmschoolshop.service.impl;

import com.zhang.ssmschoolshop.dao.ShopCartMapper;
import com.zhang.ssmschoolshop.entity.ShopCart;
import com.zhang.ssmschoolshop.entity.ShopCartExample;
import com.zhang.ssmschoolshop.entity.ShopCartKey;
import com.zhang.ssmschoolshop.service.ShopCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 购物车服务实现类
 */
@Service("addShopCart")
public class ShopCartServiceImpl implements ShopCartService {

    @Autowired(required = false)
    ShopCartMapper shopCartMapper;

    /**
     * 添加商品到购物车
     *
     * @param shopCart 购物车项
     */
    @Override
    public void addShopCart(ShopCart shopCart) {
        shopCartMapper.insertSelective(shopCart);
    }

    /**
     * 根据条件查询购物车列表
     *
     * @param shopCartExample 查询条件
     * @return 购物车列表
     */
    @Override
    public List<ShopCart> selectByExample(ShopCartExample shopCartExample) {
        return shopCartMapper.selectByExample(shopCartExample);
    }

    /**
     * 根据购物车主键删除购物车项
     *
     * @param shopCartKey 购物车主键
     */
    @Override
    public void deleteByKey(ShopCartKey shopCartKey) {
        shopCartMapper.deleteByPrimaryKey(shopCartKey);
    }

    /**
     * 根据购物车主键更新购物车项
     *
     * @param shopCart 购物车项
     */
    @Override
    public void updateCartByKey(ShopCart shopCart) {
        shopCartMapper.updateByPrimaryKeySelective(shopCart);
    }

    /**
     * 根据购物车主键查询购物车项
     *
     * @param shopCartKey 购物车主键
     * @return 购物车项
     */
    @Override
    public ShopCart selectCartByKey(ShopCartKey shopCartKey) {
        return shopCartMapper.selectByPrimaryKey(shopCartKey);
    }
}
