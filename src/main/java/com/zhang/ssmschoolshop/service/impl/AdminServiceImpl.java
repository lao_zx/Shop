package com.zhang.ssmschoolshop.service.impl;

import com.zhang.ssmschoolshop.dao.AdminMapper;
import com.zhang.ssmschoolshop.entity.Admin;
import com.zhang.ssmschoolshop.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 管理员服务实现类
 */
@Service("adminService")
public class AdminServiceImpl implements AdminService {

    @Autowired(required = false)
    private AdminMapper adminMapper;

    /**
     * 根据管理员名称查询管理员信息
     *
     * @param admin 包含管理员名称的实体
     * @return 查询到的管理员信息
     */
    @Override
    public Admin selectByName(Admin admin) {
        return adminMapper.selectByName(admin);
    }
}
