package com.zhang.ssmschoolshop.service.impl;

import com.zhang.ssmschoolshop.dao.UserMapper;
import com.zhang.ssmschoolshop.entity.User;
import com.zhang.ssmschoolshop.entity.UserExample;
import com.zhang.ssmschoolshop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户服务实现类
 */
@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired(required = false)
    private UserMapper userMapper;

    /**
     * 根据用户ID查询用户信息
     *
     * @param userId 用户ID
     * @return 用户信息
     */
    @Override
    public User selectByPrimaryKey(int userId) {
        return userMapper.selectByPrimaryKey(userId);
    }

    /**
     * 根据条件查询用户列表
     *
     * @param userExample 查询条件
     * @return 用户列表
     */
    @Override
    public List<User> selectByExample(UserExample userExample) {
        return userMapper.selectByExample(userExample);
    }

    /**
     * 插入用户信息（可选择性插入）
     *
     * @param user 用户信息
     */
    @Override
    public void insertSelective(User user) {
        userMapper.insertSelective(user);
    }

    /**
     * 根据用户ID删除用户
     *
     * @param userid 用户ID
     */
    @Override
    public void deleteUserById(Integer userid) {
        userMapper.deleteByPrimaryKey(userid);
    }

    /**
     * 根据用户ID更新用户信息（可选择性更新）
     *
     * @param user 用户信息
     */
    @Override
    public void updateByPrimaryKeySelective(User user) {
        userMapper.updateByPrimaryKeySelective(user);
    }


}
