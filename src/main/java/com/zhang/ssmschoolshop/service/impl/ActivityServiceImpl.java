package com.zhang.ssmschoolshop.service.impl;

import com.zhang.ssmschoolshop.dao.ActivityMapper;
import com.zhang.ssmschoolshop.entity.Activity;
import com.zhang.ssmschoolshop.entity.ActivityExample;
import com.zhang.ssmschoolshop.service.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 实现了 ActivityService 接口的服务类。
 * 使用 Spring 的 @Service 注解，标识为服务层组件。
 */
@Service("activityService")
public class ActivityServiceImpl implements ActivityService {

    /**
     * 自动注入 ActivityMapper 实例。
     */
    @Autowired(required = false)
    ActivityMapper activityMapper;

    /**
     * 根据条件获取所有活动信息的方法。
     *
     * @param activityExample 包含查询条件的 ActivityExample 对象。
     * @return 包含活动信息的列表。
     */
    public List<Activity> getAllActivity(ActivityExample activityExample) {
        return activityMapper.selectByExample(activityExample);
    }

    /**
     * 插入活动信息的方法，只插入非空字段。
     *
     * @param activity 包含要插入的活动信息的 Activity 对象。
     */
    @Override
    public void insertActivitySelective(Activity activity) {
        activityMapper.insertSelective(activity);
    }

    /**
     * 根据活动ID查询活动信息的方法。
     *
     * @param activityid 活动ID。
     * @return 包含查询到的活动信息的 Activity 对象。
     */
    @Override
    public Activity selectByKey(Integer activityid) {
        return activityMapper.selectByPrimaryKey(activityid);
    }

    /**
     * 根据活动ID删除活动信息的方法。
     *
     * @param activityid 活动ID。
     */
    @Override
    public void deleteByActivityId(Integer activityid) {
        activityMapper.deleteByPrimaryKey(activityid);
    }

    /*@Override
    public void updateGoodsActSelective(Goods goods) {

    }*/
}
