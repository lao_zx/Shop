package com.zhang.ssmschoolshop.service.impl;

import com.zhang.ssmschoolshop.dao.CommentMapper;
import com.zhang.ssmschoolshop.entity.Comment;
import com.zhang.ssmschoolshop.entity.CommentExample;
import com.zhang.ssmschoolshop.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 评论服务实现类
 */
@Service("commentService")
public class CommentServiceImpl implements CommentService {

    @Autowired(required = false)
    private CommentMapper commentMapper;

    /**
     * 插入评论信息（可选择性插入）
     *
     * @param comment 评论实体
     */
    @Override
    public void insertSelective(Comment comment){
        commentMapper.insertSelective(comment);
    }

    /**
     * 根据条件查询评论列表
     *
     * @param commentExample 查询条件
     * @return 评论列表
     */
    @Override
    public List<Comment> selectByExample(CommentExample commentExample) {
        return commentMapper.selectByExample(commentExample);
    }

}
