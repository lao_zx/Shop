package com.zhang.ssmschoolshop.service.impl;

import com.zhang.ssmschoolshop.dao.FavoriteMapper;
import com.zhang.ssmschoolshop.dao.GoodsMapper;
import com.zhang.ssmschoolshop.dao.ImagePathMapper;
import com.zhang.ssmschoolshop.entity.*;
import com.zhang.ssmschoolshop.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 商品服务实现类
 */
@Service("goodsService")
public class GoodsServiceImpl implements GoodsService {

    @Autowired(required = false)
    GoodsMapper goodsMapper;

    @Autowired(required = false)
    ImagePathMapper imagePathMapper;

    @Autowired(required = false)
    FavoriteMapper favoriteMapper;

    /**
     * 添加商品
     *
     * @param goods 商品对象
     * @return 商品ID
     */
    @Override
    public Integer addGoods(Goods goods) {
        goodsMapper.insertSelective(goods);
        return goods.getGoodsid();
    }

    /**
     * 添加商品图片路径
     *
     * @param imagePath 图片路径对象
     */
    @Override
    public void addImagePath(ImagePath imagePath) {
        imagePathMapper.insertSelective(imagePath);
    }

    /**
     * 根据条件查询商品列表
     *
     * @param example 查询条件
     * @return 商品列表
     */
    @Override
    public List<Goods> selectByExample(GoodsExample example) {
        return goodsMapper.selectByExampleWithBLOBs(example);
    }

    /**
     * 根据商品ID删除商品
     *
     * @param goodsid 商品ID
     */
    @Override
    public void deleteGoodsById(Integer goodsid) {
        goodsMapper.deleteByPrimaryKey(goodsid);
    }

    /**
     * 更新商品信息
     *
     * @param goods 商品对象
     */
    @Override
    public void updateGoodsById(Goods goods) {
        goodsMapper.updateByPrimaryKeySelective(goods);
    }

    /**
     * 根据商品ID查询商品图片路径列表
     *
     * @param goodsid 商品ID
     * @return 图片路径列表
     */
    @Override
    public List<ImagePath> findImagePath(Integer goodsid) {
        ImagePathExample imagePathExample = new ImagePathExample();
        imagePathExample.or().andGoodidEqualTo(goodsid);
        return imagePathMapper.selectByExample(imagePathExample);
    }

    /**
     * 根据商品ID查询商品详情
     *
     * @param goodsid 商品ID
     * @return 商品对象
     */
    @Override
    public Goods selectById(Integer goodsid) {
        return goodsMapper.selectByPrimaryKey(goodsid);
    }

    /**
     * 根据条件查询商品列表（带限制）
     *
     * @param digGoodsExample 查询条件
     * @return 商品列表
     */
    @Override
    public List<Goods> selectByExampleLimit(GoodsExample digGoodsExample) {
        return goodsMapper.selectByExampleWithBLOBsLimit(digGoodsExample);
    }

    /**
     * 添加商品收藏记录
     *
     * @param favorite 收藏对象
     */
    @Override
    public void addFavorite(Favorite favorite) {
        favoriteMapper.insertSelective(favorite);
    }

    /**
     * 根据主键查询商品收藏记录
     *
     * @param favoriteKey 收藏主键
     * @return 收藏对象
     */
    @Override
    public Favorite selectFavByKey(FavoriteKey favoriteKey) {
        return favoriteMapper.selectByPrimaryKey(favoriteKey);
    }

    /**
     * 根据主键删除商品收藏记录
     *
     * @param favoriteKey 收藏主键
     */
    @Override
    public void deleteFavByKey(FavoriteKey favoriteKey) {
        favoriteMapper.deleteByPrimaryKey(favoriteKey);
    }

    /**
     * 根据条件查询商品收藏记录列表
     *
     * @param favoriteExample 查询条件
     * @return 商品收藏记录列表
     */
    @Override
    public List<Favorite> selectFavByExample(FavoriteExample favoriteExample) {
        return favoriteMapper.selectByExample(favoriteExample);
    }
}
