package com.zhang.ssmschoolshop.service.impl;

import com.zhang.ssmschoolshop.dao.AddressMapper;
import com.zhang.ssmschoolshop.entity.Address;
import com.zhang.ssmschoolshop.entity.AddressExample;
import com.zhang.ssmschoolshop.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 地址信息服务实现类
 */
@Service("addressService")
public class AddressServiceImpl implements AddressService {

    @Autowired(required = false)
    private AddressMapper addressMapper;

    /**
     * 根据条件查询所有地址信息
     *
     * @param addressExample 查询条件
     * @return 地址信息列表
     */
    @Override
    public List<Address> getAllAddressByExample(AddressExample addressExample) {
        return addressMapper.selectByExample(addressExample);
    }

    /**
     * 根据主键选择性更新地址信息
     *
     * @param address 地址信息
     */
    @Override
    public void updateByPrimaryKeySelective(Address address) {
        addressMapper.updateByPrimaryKeySelective(address);
    }

    /**
     * 根据主键删除地址信息
     *
     * @param addressid 地址ID
     */
    @Override
    public void deleteByPrimaryKey(Integer addressid) {
        addressMapper.deleteByPrimaryKey(addressid);
    }

    /**
     * 插入地址信息
     *
     * @param address 地址信息
     */
    @Override
    public void insert(Address address) {
        addressMapper.insert(address);
    }

    /**
     * 插入地址信息（选择性）
     *
     * @param address 地址信息
     */
    @Override
    public void insertSelective(Address address) {
        addressMapper.insertSelective(address);
    }

    /**
     * 根据主键查询地址信息
     *
     * @param addressid 地址ID
     * @return 地址信息
     */
    @Override
    public Address selectByPrimaryKey(Integer addressid) {
        return addressMapper.selectByPrimaryKey(addressid);
    }
}
