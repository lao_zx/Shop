package com.zhang.ssmschoolshop.service.impl;

import com.zhang.ssmschoolshop.dao.CategoryMapper;
import com.zhang.ssmschoolshop.entity.Category;
import com.zhang.ssmschoolshop.entity.CategoryExample;
import com.zhang.ssmschoolshop.service.CateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 商品分类服务实现类
 */
@Service("cateService")
public class CateServiceImpl implements CateService {

    @Autowired(required = false)
    CategoryMapper categoryMapper;

    /**
     * 根据条件查询商品分类列表
     *
     * @param example 查询条件
     * @return 商品分类列表
     */
    @Override
    public List<Category> selectByExample(CategoryExample example) {
        return categoryMapper.selectByExample(example);
    }

    /**
     * 插入商品分类信息（可选择性插入）
     *
     * @param category 商品分类实体
     */
    @Override
    public void insertSelective(Category category) {
        categoryMapper.insertSelective(category);
    }

    /**
     * 根据条件查询一定数量的商品分类列表
     *
     * @param digCategoryExample 查询条件
     * @return 商品分类列表
     */
    @Override
    public List<Category> selectByExampleLimit(CategoryExample digCategoryExample) {
        return categoryMapper.selectByExampleLimit(digCategoryExample);
    }

    /**
     * 根据商品分类ID查询商品分类信息
     *
     * @param category 商品分类ID
     * @return 商品分类信息
     */
    @Override
    public Category selectById(Integer category) {
        return categoryMapper.selectByPrimaryKey(category);
    }

    /**
     * 更新商品分类信息（可选择性更新）
     *
     * @param category 商品分类实体
     */
    @Override
    public void updateByPrimaryKeySelective(Category category) {
        categoryMapper.updateByPrimaryKeySelective(category);
    }

    /**
     * 根据商品分类ID删除商品分类信息
     *
     * @param cateid 商品分类ID
     */
    @Override
    public void deleteByPrimaryKey(Integer cateid) {
        categoryMapper.deleteByPrimaryKey(cateid);
    }
}
