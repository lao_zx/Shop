package com.zhang.ssmschoolshop.service.impl;

import com.zhang.ssmschoolshop.dao.AddressMapper;
import com.zhang.ssmschoolshop.dao.OrderItemMapper;
import com.zhang.ssmschoolshop.dao.OrderMapper;
import com.zhang.ssmschoolshop.entity.*;
import com.zhang.ssmschoolshop.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 订单服务实现类
 */
@Service("orderService")
public class OrderServiceImpl implements OrderService {

    @Autowired(required = false)
    private OrderMapper orderMapper;

    @Autowired(required = false)
    private OrderItemMapper orderItemMapper;

    @Autowired(required = false)
    private AddressMapper addressMapper;

    /**
     * 插入订单
     *
     * @param order 订单对象
     */
    @Override
    public void insertOrder(Order order) {
        orderMapper.insertSelective(order);
    }

    /**
     * 根据订单ID删除订单
     *
     * @param orderid 订单ID
     */
    @Override
    public void deleteById(Integer orderid) {
        orderMapper.deleteByPrimaryKey(orderid);
    }

    /**
     * 根据条件查询订单列表
     *
     * @param orderExample 查询条件
     * @return 订单列表
     */
    @Override
    public List<Order> selectOrderByExample(OrderExample orderExample) {
        return orderMapper.selectByExample(orderExample);
    }

    /**
     * 根据条件查询订单项列表
     *
     * @param orderItemExample 查询条件
     * @return 订单项列表
     */
    @Override
    public List<OrderItem> getOrderItemByExample(OrderItemExample orderItemExample) {
        return orderItemMapper.selectByExample(orderItemExample);
    }

    /**
     * 根据地址ID获取地址信息
     *
     * @param addressid 地址ID
     * @return 地址信息
     */
    @Override
    public Address getAddressByKey(Integer addressid) {
        return addressMapper.selectByPrimaryKey(addressid);
    }

    /**
     * 根据订单ID更新订单信息
     *
     * @param order 订单对象
     */
    @Override
    public void updateOrderByKey(Order order) {
        orderMapper.updateByPrimaryKeySelective(order);
    }

    /**
     * 根据订单ID查询订单信息
     *
     * @param orderid 订单ID
     * @return 订单对象
     */
    @Override
    public Order selectByPrimaryKey(Integer orderid) {
        return orderMapper.selectByPrimaryKey(orderid);
    }

    /**
     * 插入订单项
     *
     * @param orderItem 订单项对象
     */
    @Override
    public void insertOrderItem(OrderItem orderItem) {
        orderItemMapper.insertSelective(orderItem);
    }
}
