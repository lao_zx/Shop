package com.zhang.ssmschoolshop.annotinon;


import java.lang.annotation.*;

/**
 * 导出实体映射注解
 *
 * 该注解用于标注实体类的字段，指定数据库列名和实体映射名，以便在导出数据时进行字段映射。
 *
 * 使用示例：
 * 1. 在实体类的字段上添加该注解，指定EnName和CnName。
 * 2. EnName表示数据库列名，CnName表示实体映射名。
 * 3. 在导出数据时，可以通过EnName和CnName进行字段映射，方便导出操作。
 *
 * 示例代码：
 * ```
 * @ExportEntityMap(EnName = "dbColumnName", CnName = "entityMappingName")
 * private String fieldName;
 * ```
 *
 * 注解属性说明：
 * - EnName: 数据库列名，默认值为"数据库列名"。
 * - CnName: 实体映射名，默认值为"实体映射名"。
 *
 * 注意：
 * 1. 该注解仅用于标记字段，并不会在运行时产生实际的影响。
 * 2. 导出数据时，需要通过反射等方式获取标记了该注解的字段，进而获取EnName和CnName进行字段映射。
 *
 * @Target: 该注解作用于字段上。
 * @Retention: 该注解在运行时有效。
 * @Documented: 生成 Javadoc 时会包含注解信息。
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ExportEntityMap {

    /**
     * 数据库列名
     */
    String EnName() default "数据库列名";

    /**
     * 实体映射名
     */
    String CnName() default "实体映射名";
}
