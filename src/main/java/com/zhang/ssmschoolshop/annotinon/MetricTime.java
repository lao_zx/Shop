package com.zhang.ssmschoolshop.annotinon;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * MetricTime 注解
 *
 * 该注解用于标注方法，表示对该方法的执行时间进行度量，通常用于性能监控或调优的场景。
 *
 * 使用示例：
 * 1. 在需要度量执行时间的方法上添加该注解。
 * 2. 通过该注解的 value 属性指定度量的名称，以便标识不同的度量点。
 * 3. 在方法执行前记录开始时间，在方法执行后记录结束时间，并计算两者的时间差，即方法的执行时间。
 *
 * 示例代码：
 * ```
 * @MetricTime("methodName")
 * public void methodName() {
 *     // 方法体
 * }
 * ```
 *
 * 注解属性说明：
 * - value: 指定度量的名称，用于标识不同的度量点，默认值为空字符串。
 *
 * 注意：
 * 1. 该注解仅用于标记方法，不会在运行时产生实际的影响。
 * 2. 度量时间的具体逻辑需要在方法执行前后进行，并根据业务需要进行相应的记录和处理。
 *
 * @Target: 该注解作用于方法上。
 * @Retention: 该注解在运行时有效。
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface MetricTime {

    /**
     * 度量的名称，用于标识不同的度量点
     */
    String value() default "";
}
