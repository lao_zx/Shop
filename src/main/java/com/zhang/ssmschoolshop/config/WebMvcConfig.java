package com.zhang.ssmschoolshop.config;


import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;



/**
 * WebMvc配置类
 *
 * 该配置类用于配置Spring MVC的相关设置，主要包括资源处理器的配置，指定文件上传的路径。
 *
 * 使用说明：
 * 1. @Configuration: 表示该类是一个配置类。
 * 2. @WebMvcConfigurer: 实现WebMvcConfigurer接口，用于自定义Spring MVC的配置。
 * 3. addResourceHandlers(): 重写该方法，配置静态资源的处理器，指定资源路径和对应的本地路径。
 *
 * 注意：
 * 1. 需要将该配置类注册到Spring容器中。
 * 2. 需要根据操作系统选择不同的文件上传路径。
 *
 * @Configuration: 该注解表示该类是一个配置类，用于配置Bean。
 * @WebMvcConfigurer: 实现该接口，用于自定义Spring MVC的配置。
 * @addResourceHandlers(): 重写该方法，配置静态资源的处理器，指定资源路径和对应的本地路径。
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    /**
     * 配置静态资源的处理器，指定资源路径和对应的本地路径
     *
     * @param registry ResourceHandlerRegistry实例，用于注册资源处理器
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        // 获取操作系统信息
        String os = System.getProperty("os.name");

        // 设置资源路径和本地路径
        String pathPatterns = "/pictures/**";
        String pathAbsolute = "file:E:/IDEAWORK/Shop/Shop/src/main/webapp/shopimg/";

        // 如果不是Windows系统，则修改本地路径
        if (!os.toLowerCase().startsWith("windows")) {
            // TODO: 需要根据实际情况修改路径，例如在Mac上的路径
            pathAbsolute = "file:/usr/upload/";
        }

        // 注册资源处理器，指定资源路径和本地路径
        registry.addResourceHandler(pathPatterns).addResourceLocations(pathAbsolute);
    }
}
