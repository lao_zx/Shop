package com.zhang.ssmschoolshop.entity;

import java.util.ArrayList;
import java.util.List;

public class ActivityExample {
    // 排序字段
    protected String orderByClause;

    // 是否去重的标志
    protected boolean distinct;

    // 存储查询条件的列表
    protected List<Criteria> oredCriteria;

    // 构造方法初始化查询条件列表
    public ActivityExample() {
        oredCriteria = new ArrayList<>();
    }

    // 设置排序字段的方法
    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    // 获取排序字段的方法
    public String getOrderByClause() {
        return orderByClause;
    }

    // 设置是否去重的方法
    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    // 获取是否去重的方法
    public boolean isDistinct() {
        return distinct;
    }

    // 获取查询条件列表的方法
    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    // 添加查询条件的方法
    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    // 创建新的查询条件的方法
    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    // 创建查询条件的方法
    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    // 内部方法，创建查询条件的方法
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    // 清除所有查询条件的方法
    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    // 用于构建单个查询条件的Criteria类
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        // 构造方法初始化查询条件列表
        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        // 检查查询条件是否有效的方法
        public boolean isValid() {
            return criteria.size() > 0;
        }

        // 获取查询条件列表的方法
        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        // 获取查询条件的方法
        public List<Criterion> getCriteria() {
            return criteria;
        }

        // 添加条件的方法
        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("查询条件的值不能为空");
            }
            criteria.add(new Criterion(condition));
        }

        // 添加带一个值的条件的方法
        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException(property + "的值不能为空");
            }
            criteria.add(new Criterion(condition, value));
        }

        // 添加带两个值的条件的方法
        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException(property + "的值不能为空");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        // 添加条件：activityId为null
        public Criteria andActivityidIsNull() {
            addCriterion("activityId is null");
            return (Criteria) this;
        }

        // 添加条件：activityId不为null
        public Criteria andActivityidIsNotNull() {
            addCriterion("activityId is not null");
            return (Criteria) this;
        }

        // 添加条件：activityId等于指定值
        public Criteria andActivityidEqualTo(Integer value) {
            addCriterion("activityId =", value, "activityid");
            return (Criteria) this;
        }

        // 添加条件：activityId不等于指定值
        public Criteria andActivityidNotEqualTo(Integer value) {
            addCriterion("activityId <>", value, "activityid");
            return (Criteria) this;
        }

        // 添加条件：activityId大于指定值
        public Criteria andActivityidGreaterThan(Integer value) {
            addCriterion("activityId >", value, "activityid");
            return (Criteria) this;
        }

        // 添加条件：activityId大于等于指定值
        public Criteria andActivityidGreaterThanOrEqualTo(Integer value) {
            addCriterion("activityId >=", value, "activityid");
            return (Criteria) this;
        }

        // 添加条件：activityId小于指定值
        public Criteria andActivityidLessThan(Integer value) {
            addCriterion("activityId <", value, "activityid");
            return (Criteria) this;
        }

        // 添加条件：activityId小于等于指定值
        public Criteria andActivityidLessThanOrEqualTo(Integer value) {
            addCriterion("activityId <=", value, "activityid");
            return (Criteria) this;
        }

        // 添加条件：activityId在指定值列表中
        public Criteria andActivityidIn(List<Integer> values) {
            addCriterion("activityId in", values, "activityid");
            return (Criteria) this;
        }

        // 添加条件：activityId不在指定值列表中
        public Criteria andActivityidNotIn(List<Integer> values) {
            addCriterion("activityId not in", values, "activityid");
            return (Criteria) this;
        }

        // 添加条件：activityId在指定范围内
        public Criteria andActivityidBetween(Integer value1, Integer value2) {
            addCriterion("activityId between", value1, value2, "activityid");
            return (Criteria) this;
        }

        // 添加条件：activityId不在指定范围内
        public Criteria andActivityidNotBetween(Integer value1, Integer value2) {
            addCriterion("activityId not between", value1, value2, "activityid");
            return (Criteria) this;
        }

        // 添加条件：activityName为null
        public Criteria andActivitynameIsNull() {
            addCriterion("activityName is null");
            return (Criteria) this;
        }

        // 添加条件：activityName不为null
        public Criteria andActivitynameIsNotNull() {
            addCriterion("activityName is not null");
            return (Criteria) this;
        }

        // 添加条件：activityName等于指定值
        public Criteria andActivitynameEqualTo(String value) {
            addCriterion("activityName =", value, "activityname");
            return (Criteria) this;
        }

        // 添加条件：activityName不等于指定值
        public Criteria andActivitynameNotEqualTo(String value) {
            addCriterion("activityName <>", value, "activityname");
            return (Criteria) this;
        }

        // 添加条件：activityName大于指定值
        public Criteria andActivitynameGreaterThan(String value) {
            addCriterion("activityName >", value, "activityname");
            return (Criteria) this;
        }

        // 添加条件：activityName大于等于指定值
        public Criteria andActivitynameGreaterThanOrEqualTo(String value) {
            addCriterion("activityName >=", value, "activityname");
            return (Criteria) this;
        }

        // 添加条件：activityName小于指定值
        public Criteria andActivitynameLessThan(String value) {
            addCriterion("activityName <", value, "activityname");
            return (Criteria) this;
        }

        // 添加条件：activityName小于等于指定值
        public Criteria andActivitynameLessThanOrEqualTo(String value) {
            addCriterion("activityName <=", value, "activityname");
            return (Criteria) this;
        }

        // 添加条件：activityName匹配指定模式
        public Criteria andActivitynameLike(String value) {
            addCriterion("activityName like", value, "activityname");
            return (Criteria) this;
        }

        // 添加条件：activityName不匹配指定模式
        public Criteria andActivitynameNotLike(String value) {
            addCriterion("activityName not like", value, "activityname");
            return (Criteria) this;
        }

        // 添加条件：activityName在指定值列表中
        public Criteria andActivitynameIn(List<String> values) {
            addCriterion("activityName in", values, "activityname");
            return (Criteria) this;
        }

        // 添加条件：activityName不在指定值列表中
        public Criteria andActivitynameNotIn(List<String> values) {
            addCriterion("activityName not in", values, "activityname");
            return (Criteria) this;
        }

        // 添加条件：activityName在指定范围内
        public Criteria andActivitynameBetween(String value1, String value2) {
            addCriterion("activityName between", value1, value2, "activityname");
            return (Criteria) this;
        }

        // 添加条件：activityName不在指定范围内
        public Criteria andActivitynameNotBetween(String value1, String value2) {
            addCriterion("activityName not between", value1, value2, "activityname");
            return (Criteria) this;
        }

        // 添加条件：activityDes为null
        public Criteria andActivitydesIsNull() {
            addCriterion("activityDes is null");
            return (Criteria) this;
        }

        // 添加条件：activityDes不为null
        public Criteria andActivitydesIsNotNull() {
            addCriterion("activityDes is not null");
            return (Criteria) this;
        }

        // 添加条件：activityDes等于指定值
        public Criteria andActivitydesEqualTo(String value) {
            addCriterion("activityDes =", value, "activitydes");
            return (Criteria) this;
        }

        // 添加条件：activityDes不等于指定值
        public Criteria andActivitydesNotEqualTo(String value) {
            addCriterion("activityDes <>", value, "activitydes");
            return (Criteria) this;
        }

        // 添加条件：activityDes大于指定值
        public Criteria andActivitydesGreaterThan(String value) {
            addCriterion("activityDes >", value, "activitydes");
            return (Criteria) this;
        }

        // 添加条件：activityDes大于等于指定值
        public Criteria andActivitydesGreaterThanOrEqualTo(String value) {
            addCriterion("activityDes >=", value, "activitydes");
            return (Criteria) this;
        }

        // 添加条件：activityDes小于指定值
        public Criteria andActivitydesLessThan(String value) {
            addCriterion("activityDes <", value, "activitydes");
            return (Criteria) this;
        }

        // 添加条件：activityDes小于等于指定值
        public Criteria andActivitydesLessThanOrEqualTo(String value) {
            addCriterion("activityDes <=", value, "activitydes");
            return (Criteria) this;
        }

        // 添加条件：activityDes匹配指定模式
        public Criteria andActivitydesLike(String value) {
            addCriterion("activityDes like", value, "activitydes");
            return (Criteria) this;
        }

        // 添加条件：activityDes不匹配指定模式
        public Criteria andActivitydesNotLike(String value) {
            addCriterion("activityDes not like", value, "activitydes");
            return (Criteria) this;
        }

        // 添加条件：activityDes在指定值列表中
        public Criteria andActivitydesIn(List<String> values) {
            addCriterion("activityDes in", values, "activitydes");
            return (Criteria) this;
        }

        // 添加条件：activityDes不在指定值列表中
        public Criteria andActivitydesNotIn(List<String> values) {
            addCriterion("activityDes not in", values, "activitydes");
            return (Criteria) this;
        }

        // 添加条件：activityDes在指定范围内
        public Criteria andActivitydesBetween(String value1, String value2) {
            addCriterion("activityDes between", value1, value2, "activitydes");
            return (Criteria) this;
        }

        // 添加条件：activityDes不在指定范围内
        public Criteria andActivitydesNotBetween(String value1, String value2) {
            addCriterion("activityDes not between", value1, value2, "activitydes");
            return (Criteria) this;
        }





        // 添加条件：discount为null
        public Criteria andDiscountIsNull() {
            addCriterion("discount is null");
            return (Criteria) this;
        }

        // 添加条件：discount不为null
        public Criteria andDiscountIsNotNull() {
            addCriterion("discount is not null");
            return (Criteria) this;
        }

        // 添加条件：discount等于指定值
        public Criteria andDiscountEqualTo(Float value) {
            addCriterion("discount =", value, "discount");
            return (Criteria) this;
        }

        // 添加条件：discount不等于指定值
        public Criteria andDiscountNotEqualTo(Float value) {
            addCriterion("discount <>", value, "discount");
            return (Criteria) this;
        }

        // 添加条件：discount大于指定值
        public Criteria andDiscountGreaterThan(Float value) {
            addCriterion("discount >", value, "discount");
            return (Criteria) this;
        }

        // 添加条件：discount大于等于指定值
        public Criteria andDiscountGreaterThanOrEqualTo(Float value) {
            addCriterion("discount >=", value, "discount");
            return (Criteria) this;
        }

        // 添加条件：discount小于指定值
        public Criteria andDiscountLessThan(Float value) {
            addCriterion("discount <", value, "discount");
            return (Criteria) this;
        }

        // 添加条件：discount小于等于指定值
        public Criteria andDiscountLessThanOrEqualTo(Float value) {
            addCriterion("discount <=", value, "discount");
            return (Criteria) this;
        }

        // 添加条件：discount在指定值列表中
        public Criteria andDiscountIn(List<Float> values) {
            addCriterion("discount in", values, "discount");
            return (Criteria) this;
        }

        // 添加条件：discount不在指定值列表中
        public Criteria andDiscountNotIn(List<Float> values) {
            addCriterion("discount not in", values, "discount");
            return (Criteria) this;
        }

        // 添加条件：discount在指定范围内
        public Criteria andDiscountBetween(Float value1, Float value2) {
            addCriterion("discount between", value1, value2, "discount");
            return (Criteria) this;
        }

        // 添加条件：discount不在指定范围内
        public Criteria andDiscountNotBetween(Float value1, Float value2) {
            addCriterion("discount not between", value1, value2, "discount");
            return (Criteria) this;
        }

        // 添加条件：fullPrice为null
        public Criteria andFullpriceIsNull() {
            addCriterion("fullPrice is null");
            return (Criteria) this;
        }

        // 添加条件：fullPrice不为null
        public Criteria andFullpriceIsNotNull() {
            addCriterion("fullPrice is not null");
            return (Criteria) this;
        }

        // 添加条件：fullPrice等于指定值
        public Criteria andFullpriceEqualTo(Integer value) {
            addCriterion("fullPrice =", value, "fullprice");
            return (Criteria) this;
        }

        // 添加条件：fullPrice不等于指定值
        public Criteria andFullpriceNotEqualTo(Integer value) {
            addCriterion("fullPrice <>", value, "fullprice");
            return (Criteria) this;
        }

        // 添加条件：fullPrice大于指定值
        public Criteria andFullpriceGreaterThan(Integer value) {
            addCriterion("fullPrice >", value, "fullprice");
            return (Criteria) this;
        }

        // 添加条件：fullPrice大于等于指定值
        public Criteria andFullpriceGreaterThanOrEqualTo(Integer value) {
            addCriterion("fullPrice >=", value, "fullprice");
            return (Criteria) this;
        }

        // 添加条件：fullPrice小于指定值
        public Criteria andFullpriceLessThan(Integer value) {
            addCriterion("fullPrice <", value, "fullprice");
            return (Criteria) this;
        }

        // 添加条件：fullPrice小于等于指定值
        public Criteria andFullpriceLessThanOrEqualTo(Integer value) {
            addCriterion("fullPrice <=", value, "fullprice");
            return (Criteria) this;
        }

        // 添加条件：fullPrice在指定值列表中
        public Criteria andFullpriceIn(List<Integer> values) {
            addCriterion("fullPrice in", values, "fullprice");
            return (Criteria) this;
        }

        // 添加条件：fullPrice不在指定值列表中
        public Criteria andFullpriceNotIn(List<Integer> values) {
            addCriterion("fullPrice not in", values, "fullprice");
            return (Criteria) this;
        }

        // 添加条件：fullPrice在指定范围内
        public Criteria andFullpriceBetween(Integer value1, Integer value2) {
            addCriterion("fullPrice between", value1, value2, "fullprice");
            return (Criteria) this;
        }

        // 添加条件：fullPrice不在指定范围内
        public Criteria andFullpriceNotBetween(Integer value1, Integer value2) {
            addCriterion("fullPrice not between", value1, value2, "fullprice");
            return (Criteria) this;
        }

        // 添加条件：reducePrice为null
        public Criteria andReducepriceIsNull() {
            addCriterion("reducePrice is null");
            return (Criteria) this;
        }

        // 添加条件：reducePrice不为null
        public Criteria andReducepriceIsNotNull() {
            addCriterion("reducePrice is not null");
            return (Criteria) this;
        }

        // 添加条件：reducePrice等于指定值
        public Criteria andReducepriceEqualTo(Integer value) {
            addCriterion("reducePrice =", value, "reduceprice");
            return (Criteria) this;
        }

        // 添加条件：reducePrice不等于指定值
        public Criteria andReducepriceNotEqualTo(Integer value) {
            addCriterion("reducePrice <>", value, "reduceprice");
            return (Criteria) this;
        }

        // 添加条件：reducePrice大于指定值
        public Criteria andReducepriceGreaterThan(Integer value) {
            addCriterion("reducePrice >", value, "reduceprice");
            return (Criteria) this;
        }

        // 添加条件：reducePrice大于等于指定值
        public Criteria andReducepriceGreaterThanOrEqualTo(Integer value) {
            addCriterion("reducePrice >=", value, "reduceprice");
            return (Criteria) this;
        }

        // 添加条件：reducePrice小于指定值
        public Criteria andReducepriceLessThan(Integer value) {
            addCriterion("reducePrice <", value, "reduceprice");
            return (Criteria) this;
        }

        // 添加条件：reducePrice小于等于指定值
        public Criteria andReducepriceLessThanOrEqualTo(Integer value) {
            addCriterion("reducePrice <=", value, "reduceprice");
            return (Criteria) this;
        }

        // 添加条件：reducePrice在指定值列表中
        public Criteria andReducepriceIn(List<Integer> values) {
            addCriterion("reducePrice in", values, "reduceprice");
            return (Criteria) this;
        }

        // 添加条件：reducePrice不在指定值列表中
        public Criteria andReducepriceNotIn(List<Integer> values) {
            addCriterion("reducePrice not in", values, "reduceprice");
            return (Criteria) this;
        }

        // 添加条件：reducePrice在指定范围内
        public Criteria andReducepriceBetween(Integer value1, Integer value2) {
            addCriterion("reducePrice between", value1, value2, "reduceprice");
            return (Criteria) this;
        }

        // 添加条件：reducePrice不在指定范围内
        public Criteria andReducepriceNotBetween(Integer value1, Integer value2) {
            addCriterion("reducePrice not between", value1, value2, "reduceprice");
            return (Criteria) this;
        }


        // 添加条件：fullNum为null
        public Criteria andFullnumIsNull() {
            addCriterion("fullNum is null");
            return (Criteria) this;
        }

        // 添加条件：fullNum不为null
        public Criteria andFullnumIsNotNull() {
            addCriterion("fullNum is not null");
            return (Criteria) this;
        }

        // 添加条件：fullNum等于指定值
        public Criteria andFullnumEqualTo(Integer value) {
            addCriterion("fullNum =", value, "fullnum");
            return (Criteria) this;
        }

        // 添加条件：fullNum不等于指定值
        public Criteria andFullnumNotEqualTo(Integer value) {
            addCriterion("fullNum <>", value, "fullnum");
            return (Criteria) this;
        }

        // 添加条件：fullNum大于指定值
        public Criteria andFullnumGreaterThan(Integer value) {
            addCriterion("fullNum >", value, "fullnum");
            return (Criteria) this;
        }

        // 添加条件：fullNum大于等于指定值
        public Criteria andFullnumGreaterThanOrEqualTo(Integer value) {
            addCriterion("fullNum >=", value, "fullnum");
            return (Criteria) this;
        }

        // 添加条件：fullNum小于指定值
        public Criteria andFullnumLessThan(Integer value) {
            addCriterion("fullNum <", value, "fullnum");
            return (Criteria) this;
        }

        // 添加条件：fullNum小于等于指定值
        public Criteria andFullnumLessThanOrEqualTo(Integer value) {
            addCriterion("fullNum <=", value, "fullnum");
            return (Criteria) this;
        }

        // 添加条件：fullNum在指定值列表中
        public Criteria andFullnumIn(List<Integer> values) {
            addCriterion("fullNum in", values, "fullnum");
            return (Criteria) this;
        }

        // 添加条件：fullNum不在指定值列表中
        public Criteria andFullnumNotIn(List<Integer> values) {
            addCriterion("fullNum not in", values, "fullnum");
            return (Criteria) this;
        }

        // 添加条件：fullNum在指定范围内
        public Criteria andFullnumBetween(Integer value1, Integer value2) {
            addCriterion("fullNum between", value1, value2, "fullnum");
            return (Criteria) this;
        }

        // 添加条件：fullNum不在指定范围内
        public Criteria andFullnumNotBetween(Integer value1, Integer value2) {
            addCriterion("fullNum not between", value1, value2, "fullnum");
            return (Criteria) this;
        }

        // 添加条件：reduceNum为null
        public Criteria andReducenumIsNull() {
            addCriterion("reduceNum is null");
            return (Criteria) this;
        }

        // 添加条件：reduceNum不为null
        public Criteria andReducenumIsNotNull() {
            addCriterion("reduceNum is not null");
            return (Criteria) this;
        }

        // 添加条件：reduceNum等于指定值
        public Criteria andReducenumEqualTo(Integer value) {
            addCriterion("reduceNum =", value, "reducenum");
            return (Criteria) this;
        }

        // 添加条件：reduceNum不等于指定值
        public Criteria andReducenumNotEqualTo(Integer value) {
            addCriterion("reduceNum <>", value, "reducenum");
            return (Criteria) this;
        }

        // 添加条件：reduceNum大于指定值
        public Criteria andReducenumGreaterThan(Integer value) {
            addCriterion("reduceNum >", value, "reducenum");
            return (Criteria) this;
        }

        // 添加条件：reduceNum大于等于指定值
        public Criteria andReducenumGreaterThanOrEqualTo(Integer value) {
            addCriterion("reduceNum >=", value, "reducenum");
            return (Criteria) this;
        }

        // 添加条件：reduceNum小于指定值
        public Criteria andReducenumLessThan(Integer value) {
            addCriterion("reduceNum <", value, "reducenum");
            return (Criteria) this;
        }

        // 添加条件：reduceNum小于等于指定值
        public Criteria andReducenumLessThanOrEqualTo(Integer value) {
            addCriterion("reduceNum <=", value, "reducenum");
            return (Criteria) this;
        }

        // 添加条件：reduceNum在指定值列表中
        public Criteria andReducenumIn(List<Integer> values) {
            addCriterion("reduceNum in", values, "reducenum");
            return (Criteria) this;
        }

        // 添加条件：reduceNum不在指定值列表中
        public Criteria andReducenumNotIn(List<Integer> values) {
            addCriterion("reduceNum not in", values, "reducenum");
            return (Criteria) this;
        }

        // 添加条件：reduceNum在指定范围内
        public Criteria andReducenumBetween(Integer value1, Integer value2) {
            addCriterion("reduceNum between", value1, value2, "reducenum");
            return (Criteria) this;
        }

        // 添加条件：reduceNum不在指定范围内
        public Criteria andReducenumNotBetween(Integer value1, Integer value2) {
            addCriterion("reduceNum not between", value1, value2, "reducenum");
            return (Criteria) this;
        }

    }

    /**
     * 用于在生成的 SQL 查询中定义条件的 Criteria 类。
     * 继承了 GeneratedCriteria 类。
     */
    public static class Criteria extends GeneratedCriteria {

        /**
         * Criteria 的默认构造函数。
         */
        protected Criteria() {
            super();
        }
    }

    /**
     * Criterion 类，表示 SQL 查询中的单个条件。
     * 封装了条件、值和类型处理器。
     */
    public static class Criterion {
        private String condition;
        private Object value;
        private Object secondValue;
        private boolean noValue;
        private boolean singleValue;
        private boolean betweenValue;
        private boolean listValue;
        private String typeHandler;

        /**
         * 获取条件。
         * @return 条件字符串。
         */
        public String getCondition() {
            return condition;
        }

        /**
         * 获取主要值。
         * @return 主要值对象。
         */
        public Object getValue() {
            return value;
        }

        /**
         * 获取第二个值。
         * @return 第二个值对象。
         */
        public Object getSecondValue() {
            return secondValue;
        }

        /**
         * 检查是否没有值。
         * @return 如果没有值则为 true。
         */
        public boolean isNoValue() {
            return noValue;
        }

        /**
         * 检查是否有单个值。
         * @return 如果有单个值则为 true。
         */
        public boolean isSingleValue() {
            return singleValue;
        }

        /**
         * 检查条件是否涉及两个值之间。
         * @return 如果涉及两个值则为 true。
         */
        public boolean isBetweenValue() {
            return betweenValue;
        }

        /**
         * 检查条件是否涉及值列表。
         * @return 如果涉及值列表则为 true。
         */
        public boolean isListValue() {
            return listValue;
        }

        /**
         * 获取类型处理器。
         * @return 类型处理器字符串。
         */
        public String getTypeHandler() {
            return typeHandler;
        }

        /**
         * 仅有条件的构造函数。
         * @param condition 条件字符串。
         */
        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        /**
         * 包含条件、值和类型处理器的构造函数。
         * @param condition 条件字符串。
         * @param value 主要值对象。
         * @param typeHandler 类型处理器字符串。
         */
        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        /**
         * 包含条件和值的构造函数。
         * @param condition 条件字符串。
         * @param value 主要值对象。
         */
        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        /**
         * 包含条件、主要值、第二个值和类型处理器的构造函数。
         * @param condition 条件字符串。
         * @param value 主要值对象。
         * @param secondValue 第二个值对象。
         * @param typeHandler 类型处理器字符串。
         */
        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        /**
         * 包含条件、主要值和第二个值的构造函数。
         * @param condition 条件字符串。
         * @param value 主要值对象。
         * @param secondValue 第二个值对象。
         */
        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }

}