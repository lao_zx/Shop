package com.zhang.ssmschoolshop.util;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.UUID;


/**
 * ImageUtil是一个用于处理图片上传的工具类，提供了保存上传图片的方法。
 */
public class ImageUtil {

    /**
     * 保存上传的图片文件
     *
     * @param file     上传的MultipartFile对象，包含上传的文件信息
     * @param shopName 商店名称，用于生成文件名
     * @return 如果保存成功，返回保存的文件名；否则返回"false"
     */
    public static String imagePath(MultipartFile file, String shopName) {
        if (file.isEmpty()) {
            return "false";
        }
        int size = (int) file.getSize();
        String path = "E:/IDEAWORK/Shop/Shop/src/main/webapp/shopimg";  // 设置保存路径，默认为Windows系统
        String os = System.getProperty("os.name");
        if (!os.toLowerCase().startsWith("windows")) {
            // 如果不是Windows系统，修改保存路径为Linux/Mac系统
            // 注意：在实际项目中，需要根据具体情况修改为服务器上的真实路径
            path = "/usr/upload";
        }
        // 生成文件名，使用UUID和商店名称
        String fileName = UUID.randomUUID().toString().substring(0, 4) + shopName;
        // 创建目标文件
        File dest = new File(path + "/" + fileName);
        System.out.println("保存的绝对路径为:" + dest);
        if (!dest.getParentFile().exists()) { //判断文件父目录是否存在，不存在则创建
            dest.getParentFile().mkdir();
        }
        try {
            //保存文件到目标路径
            file.transferTo(dest);
            return fileName;
        } catch (IllegalStateException e) {
            // 文件保存失败，打印异常信息
            e.printStackTrace();
            return "false";
        } catch (IOException e) {
            // 文件保存失败，打印异常信息
            e.printStackTrace();
            return "false";
        }
    }
}
