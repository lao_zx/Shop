package com.zhang.ssmschoolshop.util;

import java.security.MessageDigest;


/**
 * Md5Util是一个用于MD5加密的工具类，提供了对字符串进行MD5加密的方法。
 */
public class Md5Util {

    // 用于将byte转换为16进制表示的字符
    private static final String hexDigIts[] = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};

    /**
     * 对字符串进行MD5加密
     *
     * @param origin       要加密的字符串
     * @param charsetname  字符串的编码方式
     * @return 经过MD5加密后的字符串
     */
    public static String MD5Encode(String origin, String charsetname) {
        String resultString = null;
        try {
            resultString = new String(origin);
            MessageDigest md = MessageDigest.getInstance("MD5");
            resultString = byteArrayToHexString(md.digest(resultString.getBytes(charsetname)));
        } catch (Exception e) {
            // 异常处理，可以根据具体情况进行适当处理
        }
        return resultString;
    }

    /**
     * 将字节数组转换为16进制字符串
     *
     * @param b 字节数组
     * @return 转换后的16进制字符串
     */
    public static String byteArrayToHexString(byte b[]) {
        StringBuffer resultSb = new StringBuffer();
        for (int i = 0; i < b.length; i++) {
            resultSb.append(byteToHexString(b[i]));
        }
        return resultSb.toString();
    }

    /**
     * 将字节转换为16进制字符串
     *
     * @param b 字节
     * @return 转换后的16进制字符串
     */
    public static String byteToHexString(byte b) {
        int n = b;
        if (n < 0) {
            n += 256;
        }
        int d1 = n / 16;
        int d2 = n % 16;
        return hexDigIts[d1] + hexDigIts[d2];
    }
}
