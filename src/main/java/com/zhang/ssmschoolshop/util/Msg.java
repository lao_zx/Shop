package com.zhang.ssmschoolshop.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Msg类是一个通用的响应消息类，用于封装服务器向客户端返回的信息。
 * - 成功状态码：100
 * - 失败状态码：200
 */
public class Msg {

    // 状态码，100表示成功，200表示失败
    private int code;

    // 提示信息
    private String msg;

    // 存储数据的Map
    private Map<String, Object> info = new HashMap<>();

    /**
     * 获取一个表示成功的Msg对象
     * @param msg 提示信息
     * @return Msg对象，表示操作成功
     */
    public static Msg success(String msg) {
        Msg result = new Msg();
        result.setCode(100);
        result.setMsg(msg);
        return result;
    }

    /**
     * 获取一个表示失败的Msg对象
     * @param msg 提示信息
     * @return Msg对象，表示操作失败
     */
    public static Msg fail(String msg) {
        Msg result = new Msg();
        result.setCode(200);
        result.setMsg(msg);
        return result;
    }

    /**
     * 向Msg对象中添加键值对
     * @param key 键
     * @param value 值
     * @return Msg对象，支持链式调用
     */
    public Msg add(String key, Object value) {
        this.getInfo().put(key, value);
        return this;
    }

    // 下面是Getter和Setter方法...

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Map<String, Object> getInfo() {
        return info;
    }

    public void setInfo(Map<String, Object> info) {
        this.info = info;
    }
}
