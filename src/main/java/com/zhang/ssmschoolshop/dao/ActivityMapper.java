package com.zhang.ssmschoolshop.dao;

import com.zhang.ssmschoolshop.entity.Activity;
import com.zhang.ssmschoolshop.entity.ActivityExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 与活动信息相关的数据库操作接口。
 */
public interface ActivityMapper {

    /**
     * 根据指定条件统计符合条件的活动数量。
     *
     * @param example 活动条件的查询示例。
     * @return 活动数量。
     */
    long countByExample(ActivityExample example);

    /**
     * 根据指定条件删除符合条件的活动信息。
     *
     * @param example 活动条件的查询示例。
     * @return 受影响的行数。
     */
    int deleteByExample(ActivityExample example);

    /**
     * 根据活动ID删除对应的活动信息。
     *
     * @param activityid 活动ID。
     * @return 受影响的行数。
     */
    int deleteByPrimaryKey(Integer activityid);

    /**
     * 插入一条活动信息记录。
     *
     * @param record 待插入的活动信息。
     * @return 受影响的行数。
     */
    int insert(Activity record);

    /**
     * 插入一条活动信息记录，只插入非空字段。
     *
     * @param record 待插入的活动信息。
     * @return 受影响的行数。
     */
    int insertSelective(Activity record);

    /**
     * 根据指定条件查询符合条件的活动信息列表。
     *
     * @param example 活动条件的查询示例。
     * @return 活动信息列表。
     */
    List<Activity> selectByExample(ActivityExample example);

    /**
     * 根据活动ID查询对应的活动信息。
     *
     * @param activityid 活动ID。
     * @return 活动信息。
     */
    Activity selectByPrimaryKey(Integer activityid);

    /**
     * 根据指定条件更新符合条件的活动信息，只更新非空字段。
     *
     * @param record  待更新的活动信息。
     * @param example 活动条件的查询示例。
     * @return 受影响的行数。
     */
    int updateByExampleSelective(@Param("record") Activity record, @Param("example") ActivityExample example);

    /**
     * 根据指定条件更新符合条件的活动信息。
     *
     * @param record  待更新的活动信息。
     * @param example 活动条件的查询示例。
     * @return 受影响的行数。
     */
    int updateByExample(@Param("record") Activity record, @Param("example") ActivityExample example);

    /**
     * 根据活动ID更新对应的活动信息，只更新非空字段。
     *
     * @param record 待更新的活动信息。
     * @return 受影响的行数。
     */
    int updateByPrimaryKeySelective(Activity record);

    /**
     * 根据活动ID更新对应的活动信息。
     *
     * @param record 待更新的活动信息。
     * @return 受影响的行数。
     */
    int updateByPrimaryKey(Activity record);
}
