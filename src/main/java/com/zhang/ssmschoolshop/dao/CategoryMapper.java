package com.zhang.ssmschoolshop.dao;

import com.zhang.ssmschoolshop.entity.Category;
import com.zhang.ssmschoolshop.entity.CategoryExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 商品分类信息数据访问接口。
 */
public interface CategoryMapper {

    /**
     * 根据指定的条件统计商品分类数量。
     *
     * @param example 商品分类条件的查询示例。
     * @return 商品分类数量。
     */
    long countByExample(CategoryExample example);

    /**
     * 根据指定的条件删除商品分类信息。
     *
     * @param example 商品分类条件的查询示例。
     * @return 受影响的行数。
     */
    int deleteByExample(CategoryExample example);

    /**
     * 根据商品分类ID删除对应的商品分类信息。
     *
     * @param cateid 商品分类ID。
     * @return 受影响的行数。
     */
    int deleteByPrimaryKey(Integer cateid);

    /**
     * 插入一条商品分类信息记录。
     *
     * @param record 待插入的商品分类信息。
     * @return 受影响的行数。
     */
    int insert(Category record);

    /**
     * 插入一条商品分类信息记录，只插入非空字段。
     *
     * @param record 待插入的商品分类信息。
     * @return 受影响的行数。
     */
    int insertSelective(Category record);

    /**
     * 根据指定的条件查询商品分类信息列表。
     *
     * @param example 商品分类条件的查询示例。
     * @return 商品分类信息列表。
     */
    List<Category> selectByExample(CategoryExample example);

    /**
     * 根据指定的条件查询商品分类信息列表（限制返回结果的数量）。
     *
     * @param example 商品分类条件的查询示例。
     * @return 商品分类信息列表。
     */
    List<Category> selectByExampleLimit(CategoryExample example);

    /**
     * 根据商品分类ID查询对应的商品分类信息。
     *
     * @param cateid 商品分类ID。
     * @return 商品分类信息。
     */
    Category selectByPrimaryKey(Integer cateid);

    /**
     * 根据指定的条件更新商品分类信息，只更新非空字段。
     *
     * @param record  待更新的商品分类信息。
     * @param example 商品分类条件的查询示例。
     * @return 受影响的行数。
     */
    int updateByExampleSelective(@Param("record") Category record, @Param("example") CategoryExample example);

    /**
     * 根据指定的条件更新商品分类信息。
     *
     * @param record  待更新的商品分类信息。
     * @param example 商品分类条件的查询示例。
     * @return 受影响的行数。
     */
    int updateByExample(@Param("record") Category record, @Param("example") CategoryExample example);

    /**
     * 根据商品分类ID更新对应的商品分类信息，只更新非空字段。
     *
     * @param record 待更新的商品分类信息。
     * @return 受影响的行数。
     */
    int updateByPrimaryKeySelective(Category record);

    /**
     * 根据商品分类ID更新对应的商品分类信息。
     *
     * @param record 待更新的商品分类信息。
     * @return 受影响的行数。
     */
    int updateByPrimaryKey(Category record);
}
