package com.zhang.ssmschoolshop.dao;

import com.zhang.ssmschoolshop.entity.Comment;
import com.zhang.ssmschoolshop.entity.CommentExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 评论信息数据访问接口。
 */
public interface CommentMapper {

    /**
     * 根据指定的条件统计评论数量。
     *
     * @param example 评论条件的查询示例。
     * @return 评论数量。
     */
    long countByExample(CommentExample example);

    /**
     * 根据指定的条件删除评论信息。
     *
     * @param example 评论条件的查询示例。
     * @return 受影响的行数。
     */
    int deleteByExample(CommentExample example);

    /**
     * 根据评论ID删除对应的评论信息。
     *
     * @param commentid 评论ID。
     * @return 受影响的行数。
     */
    int deleteByPrimaryKey(Integer commentid);

    /**
     * 插入一条评论信息记录。
     *
     * @param record 待插入的评论信息。
     * @return 受影响的行数。
     */
    int insert(Comment record);

    /**
     * 插入一条评论信息记录，只插入非空字段。
     *
     * @param record 待插入的评论信息。
     * @return 受影响的行数。
     */
    int insertSelective(Comment record);

    /**
     * 根据指定的条件查询评论信息列表。
     *
     * @param example 评论条件的查询示例。
     * @return 评论信息列表。
     */
    List<Comment> selectByExample(CommentExample example);

    /**
     * 根据评论ID查询对应的评论信息。
     *
     * @param commentid 评论ID。
     * @return 评论信息。
     */
    Comment selectByPrimaryKey(Integer commentid);

    /**
     * 根据指定的条件更新评论信息，只更新非空字段。
     *
     * @param record  待更新的评论信息。
     * @param example 评论条件的查询示例。
     * @return 受影响的行数。
     */
    int updateByExampleSelective(@Param("record") Comment record, @Param("example") CommentExample example);

    /**
     * 根据指定的条件更新评论信息。
     *
     * @param record  待更新的评论信息。
     * @param example 评论条件的查询示例。
     * @return 受影响的行数。
     */
    int updateByExample(@Param("record") Comment record, @Param("example") CommentExample example);

    /**
     * 根据评论ID更新对应的评论信息，只更新非空字段。
     *
     * @param record 待更新的评论信息。
     * @return 受影响的行数。
     */
    int updateByPrimaryKeySelective(Comment record);

    /**
     * 根据评论ID更新对应的评论信息。
     *
     * @param record 待更新的评论信息。
     * @return 受影响的行数。
     */
    int updateByPrimaryKey(Comment record);
}
