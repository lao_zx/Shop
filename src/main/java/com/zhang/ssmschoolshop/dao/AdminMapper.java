package com.zhang.ssmschoolshop.dao;

import org.apache.ibatis.annotations.Param;
import com.zhang.ssmschoolshop.entity.Admin;
import com.zhang.ssmschoolshop.entity.AdminExample;

import java.util.List;

/**
 * 管理员信息数据访问接口。
 */
public interface AdminMapper {

    /**
     * 根据指定的条件统计管理员数量。
     *
     * @param example 管理员条件的查询示例。
     * @return 管理员数量。
     */
    long countByExample(AdminExample example);

    /**
     * 根据指定的条件删除管理员信息。
     *
     * @param example 管理员条件的查询示例。
     * @return 受影响的行数。
     */
    int deleteByExample(AdminExample example);

    /**
     * 根据管理员ID删除对应的管理员信息。
     *
     * @param adminid 管理员ID。
     * @return 受影响的行数。
     */
    int deleteByPrimaryKey(Integer adminid);

    /**
     * 插入一条管理员信息记录。
     *
     * @param record 待插入的管理员信息。
     * @return 受影响的行数。
     */
    int insert(Admin record);

    /**
     * 插入一条管理员信息记录，只插入非空字段。
     *
     * @param record 待插入的管理员信息。
     * @return 受影响的行数。
     */
    int insertSelective(Admin record);

    /**
     * 根据指定的条件查询管理员信息列表。
     *
     * @param example 管理员条件的查询示例。
     * @return 管理员信息列表。
     */
    List<Admin> selectByExample(AdminExample example);

    /**
     * 根据管理员ID查询对应的管理员信息。
     *
     * @param adminid 管理员ID。
     * @return 管理员信息。
     */
    Admin selectByPrimaryKey(Integer adminid);

    /**
     * 根据管理员名称查询对应的管理员信息。
     *
     * @param admin 管理员信息。
     * @return 管理员信息。
     */
    Admin selectByName(Admin admin);

    /**
     * 根据指定的条件更新管理员信息，只更新非空字段。
     *
     * @param record  待更新的管理员信息。
     * @param example 管理员条件的查询示例。
     * @return 受影响的行数。
     */
    int updateByExampleSelective(@Param("record") Admin record, @Param("example") AdminExample example);

    /**
     * 根据指定的条件更新管理员信息。
     *
     * @param record  待更新的管理员信息。
     * @param example 管理员条件的查询示例。
     * @return 受影响的行数。
     */
    int updateByExample(@Param("record") Admin record, @Param("example") AdminExample example);

    /**
     * 根据管理员ID更新对应的管理员信息，只更新非空字段。
     *
     * @param record 待更新的管理员信息。
     * @return 受影响的行数。
     */
    int updateByPrimaryKeySelective(Admin record);

    /**
     * 根据管理员ID更新对应的管理员信息。
     *
     * @param record 待更新的管理员信息。
     * @return 受影响的行数。
     */
    int updateByPrimaryKey(Admin record);
}
