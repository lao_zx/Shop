package com.zhang.ssmschoolshop.dao;

import com.zhang.ssmschoolshop.entity.ShopCart;
import com.zhang.ssmschoolshop.entity.ShopCartExample;
import com.zhang.ssmschoolshop.entity.ShopCartKey;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 购物车数据访问接口。
 */
public interface ShopCartMapper {

    /**
     * 根据指定的条件统计购物车数量。
     *
     * @param example 购物车条件的查询示例。
     * @return 购物车数量。
     */
    long countByExample(ShopCartExample example);

    /**
     * 根据指定的条件删除购物车信息。
     *
     * @param example 购物车条件的查询示例。
     * @return 受影响的行数。
     */
    int deleteByExample(ShopCartExample example);

    /**
     * 根据购物车键删除对应的购物车信息。
     *
     * @param key 购物车键。
     * @return 受影响的行数。
     */
    int deleteByPrimaryKey(ShopCartKey key);

    /**
     * 插入一条购物车信息记录。
     *
     * @param record 待插入的购物车信息。
     * @return 受影响的行数。
     */
    int insert(ShopCart record);

    /**
     * 插入一条购物车信息记录，只插入非空字段。
     *
     * @param record 待插入的购物车信息。
     * @return 受影响的行数。
     */
    int insertSelective(ShopCart record);

    /**
     * 根据指定的条件查询购物车信息列表。
     *
     * @param example 购物车条件的查询示例。
     * @return 购物车信息列表。
     */
    List<ShopCart> selectByExample(ShopCartExample example);

    /**
     * 根据购物车键查询对应的购物车信息。
     *
     * @param key 购物车键。
     * @return 购物车信息。
     */
    ShopCart selectByPrimaryKey(ShopCartKey key);

    /**
     * 根据指定的条件更新购物车信息，只更新非空字段。
     *
     * @param record  待更新的购物车信息。
     * @param example 购物车条件的查询示例。
     * @return 受影响的行数。
     */
    int updateByExampleSelective(@Param("record") ShopCart record, @Param("example") ShopCartExample example);

    /**
     * 根据指定的条件更新购物车信息。
     *
     * @param record  待更新的购物车信息。
     * @param example 购物车条件的查询示例。
     * @return 受影响的行数。
     */
    int updateByExample(@Param("record") ShopCart record, @Param("example") ShopCartExample example);

    /**
     * 根据购物车键更新对应的购物车信息，只更新非空字段。
     *
     * @param record 待更新的购物车信息。
     * @return 受影响的行数。
     */
    int updateByPrimaryKeySelective(ShopCart record);

    /**
     * 根据购物车键更新对应的购物车信息。
     *
     * @param record 待更新的购物车信息。
     * @return 受影响的行数。
     */
    int updateByPrimaryKey(ShopCart record);
}
