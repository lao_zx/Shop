package com.zhang.ssmschoolshop.dao;

import com.zhang.ssmschoolshop.entity.Favorite;
import com.zhang.ssmschoolshop.entity.FavoriteExample;
import com.zhang.ssmschoolshop.entity.FavoriteKey;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 收藏信息数据访问接口。
 */
public interface FavoriteMapper {

    /**
     * 根据指定的条件统计收藏数量。
     *
     * @param example 收藏条件的查询示例。
     * @return 收藏数量。
     */
    long countByExample(FavoriteExample example);

    /**
     * 根据指定的条件删除收藏信息。
     *
     * @param example 收藏条件的查询示例。
     * @return 受影响的行数。
     */
    int deleteByExample(FavoriteExample example);

    /**
     * 根据收藏Key删除对应的收藏信息。
     *
     * @param key 收藏Key。
     * @return 受影响的行数。
     */
    int deleteByPrimaryKey(FavoriteKey key);

    /**
     * 插入一条收藏信息记录。
     *
     * @param record 待插入的收藏信息。
     * @return 受影响的行数。
     */
    int insert(Favorite record);

    /**
     * 插入一条收藏信息记录，只插入非空字段。
     *
     * @param record 待插入的收藏信息。
     * @return 受影响的行数。
     */
    int insertSelective(Favorite record);

    /**
     * 根据指定的条件查询收藏信息列表。
     *
     * @param example 收藏条件的查询示例。
     * @return 收藏信息列表。
     */
    List<Favorite> selectByExample(FavoriteExample example);

    /**
     * 根据收藏Key查询对应的收藏信息。
     *
     * @param key 收藏Key。
     * @return 收藏信息。
     */
    Favorite selectByPrimaryKey(FavoriteKey key);

    /**
     * 根据指定的条件更新收藏信息，只更新非空字段。
     *
     * @param record  待更新的收藏信息。
     * @param example 收藏条件的查询示例。
     * @return 受影响的行数。
     */
    int updateByExampleSelective(@Param("record") Favorite record, @Param("example") FavoriteExample example);

    /**
     * 根据指定的条件更新收藏信息。
     *
     * @param record  待更新的收藏信息。
     * @param example 收藏条件的查询示例。
     * @return 受影响的行数。
     */
    int updateByExample(@Param("record") Favorite record, @Param("example") FavoriteExample example);

    /**
     * 根据收藏Key更新对应的收藏信息，只更新非空字段。
     *
     * @param record 待更新的收藏信息。
     * @return 受影响的行数。
     */
    int updateByPrimaryKeySelective(Favorite record);

    /**
     * 根据收藏Key更新对应的收藏信息。
     *
     * @param record 待更新的收藏信息。
     * @return 受影响的行数。
     */
    int updateByPrimaryKey(Favorite record);
}
