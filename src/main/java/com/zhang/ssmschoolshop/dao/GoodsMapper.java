package com.zhang.ssmschoolshop.dao;

import org.apache.ibatis.annotations.Param;
import com.zhang.ssmschoolshop.entity.Goods;
import com.zhang.ssmschoolshop.entity.GoodsExample;

import java.util.List;

/**
 * 商品信息数据访问接口。
 */
public interface GoodsMapper {

    /**
     * 根据指定的条件统计商品数量。
     *
     * @param example 商品条件的查询示例。
     * @return 商品数量。
     */
    long countByExample(GoodsExample example);

    /**
     * 根据指定的条件删除商品信息。
     *
     * @param example 商品条件的查询示例。
     * @return 受影响的行数。
     */
    int deleteByExample(GoodsExample example);

    /**
     * 根据商品ID删除对应的商品信息。
     *
     * @param goodsid 商品ID。
     * @return 受影响的行数。
     */
    int deleteByPrimaryKey(Integer goodsid);

    /**
     * 插入一条商品信息记录。
     *
     * @param record 待插入的商品信息。
     * @return 受影响的行数。
     */
    int insert(Goods record);

    /**
     * 插入一条商品信息记录，只插入非空字段。
     *
     * @param record 待插入的商品信息。
     * @return 受影响的行数。
     */
    int insertSelective(Goods record);

    /**
     * 根据指定的条件查询商品信息列表（包含大字段）。
     *
     * @param example 商品条件的查询示例。
     * @return 商品信息列表。
     */
    List<Goods> selectByExampleWithBLOBs(GoodsExample example);

    /**
     * 根据指定的条件查询商品信息列表。
     *
     * @param example 商品条件的查询示例。
     * @return 商品信息列表。
     */
    List<Goods> selectByExample(GoodsExample example);

    /**
     * 根据商品ID查询对应的商品信息。
     *
     * @param goodsid 商品ID。
     * @return 商品信息。
     */
    Goods selectByPrimaryKey(Integer goodsid);

    /**
     * 根据指定的条件更新商品信息，只更新非空字段。
     *
     * @param record  待更新的商品信息。
     * @param example 商品条件的查询示例。
     * @return 受影响的行数。
     */
    int updateByExampleSelective(@Param("record") Goods record, @Param("example") GoodsExample example);

    /**
     * 根据指定的条件更新商品信息（包含大字段），只更新非空字段。
     *
     * @param record  待更新的商品信息。
     * @param example 商品条件的查询示例。
     * @return 受影响的行数。
     */
    int updateByExampleWithBLOBs(@Param("record") Goods record, @Param("example") GoodsExample example);

    /**
     * 根据指定的条件更新商品信息。
     *
     * @param record  待更新的商品信息。
     * @param example 商品条件的查询示例。
     * @return 受影响的行数。
     */
    int updateByExample(@Param("record") Goods record, @Param("example") GoodsExample example);

    /**
     * 根据商品ID更新对应的商品信息，只更新非空字段。
     *
     * @param record 待更新的商品信息。
     * @return 受影响的行数。
     */
    int updateByPrimaryKeySelective(Goods record);

    /**
     * 根据商品ID更新对应的商品信息（包含大字段），只更新非空字段。
     *
     * @param record 待更新的商品信息。
     * @return 受影响的行数。
     */
    int updateByPrimaryKeyWithBLOBs(Goods record);

    /**
     * 根据商品ID更新对应的商品信息。
     *
     * @param record 待更新的商品信息。
     * @return 受影响的行数。
     */
    int updateByPrimaryKey(Goods record);

    /**
     * 根据指定的条件查询商品信息列表（包含大字段）并限制结果数量。
     *
     * @param example 商品条件的查询示例。
     * @return 商品信息列表。
     */
    List<Goods> selectByExampleWithBLOBsLimit(GoodsExample example);
}
