package com.zhang.ssmschoolshop.dao;

import com.zhang.ssmschoolshop.entity.ImagePath;
import com.zhang.ssmschoolshop.entity.ImagePathExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 商品图片路径数据访问接口。
 */
public interface ImagePathMapper {

    /**
     * 根据指定的条件统计商品图片路径数量。
     *
     * @param example 商品图片路径条件的查询示例。
     * @return 商品图片路径数量。
     */
    long countByExample(ImagePathExample example);

    /**
     * 根据指定的条件删除商品图片路径信息。
     *
     * @param example 商品图片路径条件的查询示例。
     * @return 受影响的行数。
     */
    int deleteByExample(ImagePathExample example);

    /**
     * 根据商品图片路径ID删除对应的商品图片路径信息。
     *
     * @param pathid 商品图片路径ID。
     * @return 受影响的行数。
     */
    int deleteByPrimaryKey(Integer pathid);

    /**
     * 插入一条商品图片路径信息记录。
     *
     * @param record 待插入的商品图片路径信息。
     * @return 受影响的行数。
     */
    int insert(ImagePath record);

    /**
     * 插入一条商品图片路径信息记录，只插入非空字段。
     *
     * @param record 待插入的商品图片路径信息。
     * @return 受影响的行数。
     */
    int insertSelective(ImagePath record);

    /**
     * 根据指定的条件查询商品图片路径信息列表。
     *
     * @param example 商品图片路径条件的查询示例。
     * @return 商品图片路径信息列表。
     */
    List<ImagePath> selectByExample(ImagePathExample example);

    /**
     * 根据商品图片路径ID查询对应的商品图片路径信息。
     *
     * @param pathid 商品图片路径ID。
     * @return 商品图片路径信息。
     */
    ImagePath selectByPrimaryKey(Integer pathid);

    /**
     * 根据指定的条件更新商品图片路径信息，只更新非空字段。
     *
     * @param record  待更新的商品图片路径信息。
     * @param example 商品图片路径条件的查询示例。
     * @return 受影响的行数。
     */
    int updateByExampleSelective(@Param("record") ImagePath record, @Param("example") ImagePathExample example);

    /**
     * 根据指定的条件更新商品图片路径信息。
     *
     * @param record  待更新的商品图片路径信息。
     * @param example 商品图片路径条件的查询示例。
     * @return 受影响的行数。
     */
    int updateByExample(@Param("record") ImagePath record, @Param("example") ImagePathExample example);

    /**
     * 根据商品图片路径ID更新对应的商品图片路径信息，只更新非空字段。
     *
     * @param record 待更新的商品图片路径信息。
     * @return 受影响的行数。
     */
    int updateByPrimaryKeySelective(ImagePath record);

    /**
     * 根据商品图片路径ID更新对应的商品图片路径信息。
     *
     * @param record 待更新的商品图片路径信息。
     * @return 受影响的行数。
     */
    int updateByPrimaryKey(ImagePath record);
}
