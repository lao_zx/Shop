package com.zhang.ssmschoolshop.dao;

import com.zhang.ssmschoolshop.entity.User;
import com.zhang.ssmschoolshop.entity.UserExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户数据访问接口。
 */
public interface UserMapper {

    /**
     * 根据指定的条件统计用户数量。
     *
     * @param example 用户条件的查询示例。
     * @return 用户数量。
     */
    long countByExample(UserExample example);

    /**
     * 根据指定的条件删除用户信息。
     *
     * @param example 用户条件的查询示例。
     * @return 受影响的行数。
     */
    int deleteByExample(UserExample example);

    /**
     * 根据用户ID删除对应的用户信息。
     *
     * @param userid 用户ID。
     * @return 受影响的行数。
     */
    int deleteByPrimaryKey(Integer userid);

    /**
     * 插入一条用户信息记录。
     *
     * @param record 待插入的用户信息。
     * @return 受影响的行数。
     */
    int insert(User record);

    /**
     * 插入一条用户信息记录，只插入非空字段。
     *
     * @param record 待插入的用户信息。
     * @return 受影响的行数。
     */
    int insertSelective(User record);

    /**
     * 根据指定的条件查询用户信息列表。
     *
     * @param example 用户条件的查询示例。
     * @return 用户信息列表。
     */
    List<User> selectByExample(UserExample example);

    /**
     * 根据用户ID查询对应的用户信息。
     *
     * @param userid 用户ID。
     * @return 用户信息。
     */
    User selectByPrimaryKey(Integer userid);

    /**
     * 根据指定的条件更新用户信息，只更新非空字段。
     *
     * @param record  待更新的用户信息。
     * @param example 用户条件的查询示例。
     * @return 受影响的行数。
     */
    int updateByExampleSelective(@Param("record") User record, @Param("example") UserExample example);

    /**
     * 根据指定的条件更新用户信息。
     *
     * @param record  待更新的用户信息。
     * @param example 用户条件的查询示例。
     * @return 受影响的行数。
     */
    int updateByExample(@Param("record") User record, @Param("example") UserExample example);

    /**
     * 根据用户ID更新对应的用户信息，只更新非空字段。
     *
     * @param record 待更新的用户信息。
     * @return 受影响的行数。
     */
    int updateByPrimaryKeySelective(User record);

    /**
     * 根据用户ID更新对应的用户信息。
     *
     * @param record 待更新的用户信息。
     * @return 受影响的行数。
     */
    int updateByPrimaryKey(User record);
}
