package com.zhang.ssmschoolshop.dao;

import com.zhang.ssmschoolshop.entity.Order;
import com.zhang.ssmschoolshop.entity.OrderExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 订单数据访问接口。
 */
public interface OrderMapper {

    /**
     * 根据指定的条件统计订单数量。
     *
     * @param example 订单条件的查询示例。
     * @return 订单数量。
     */
    long countByExample(OrderExample example);

    /**
     * 根据指定的条件删除订单信息。
     *
     * @param example 订单条件的查询示例。
     * @return 受影响的行数。
     */
    int deleteByExample(OrderExample example);

    /**
     * 根据订单ID删除对应的订单信息。
     *
     * @param orderid 订单ID。
     * @return 受影响的行数。
     */
    int deleteByPrimaryKey(Integer orderid);

    /**
     * 插入一条订单信息记录。
     *
     * @param record 待插入的订单信息。
     * @return 受影响的行数。
     */
    int insert(Order record);

    /**
     * 插入一条订单信息记录，只插入非空字段。
     *
     * @param record 待插入的订单信息。
     * @return 受影响的行数。
     */
    int insertSelective(Order record);

    /**
     * 根据指定的条件查询订单信息列表。
     *
     * @param example 订单条件的查询示例。
     * @return 订单信息列表。
     */
    List<Order> selectByExample(OrderExample example);

    /**
     * 根据订单ID查询对应的订单信息。
     *
     * @param orderid 订单ID。
     * @return 订单信息。
     */
    Order selectByPrimaryKey(Integer orderid);

    /**
     * 根据指定的条件更新订单信息，只更新非空字段。
     *
     * @param record  待更新的订单信息。
     * @param example 订单条件的查询示例。
     * @return 受影响的行数。
     */
    int updateByExampleSelective(@Param("record") Order record, @Param("example") OrderExample example);

    /**
     * 根据指定的条件更新订单信息。
     *
     * @param record  待更新的订单信息。
     * @param example 订单条件的查询示例。
     * @return 受影响的行数。
     */
    int updateByExample(@Param("record") Order record, @Param("example") OrderExample example);

    /**
     * 根据订单ID更新对应的订单信息，只更新非空字段。
     *
     * @param record 待更新的订单信息。
     * @return 受影响的行数。
     */
    int updateByPrimaryKeySelective(Order record);

    /**
     * 根据订单ID更新对应的订单信息。
     *
     * @param record 待更新的订单信息。
     * @return 受影响的行数。
     */
    int updateByPrimaryKey(Order record);
}
