package com.zhang.ssmschoolshop.dao;

import com.zhang.ssmschoolshop.entity.OrderItem;
import com.zhang.ssmschoolshop.entity.OrderItemExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 订单项数据访问接口。
 */
public interface OrderItemMapper {

    /**
     * 根据指定的条件统计订单项数量。
     *
     * @param example 订单项条件的查询示例。
     * @return 订单项数量。
     */
    long countByExample(OrderItemExample example);

    /**
     * 根据指定的条件删除订单项信息。
     *
     * @param example 订单项条件的查询示例。
     * @return 受影响的行数。
     */
    int deleteByExample(OrderItemExample example);

    /**
     * 根据订单项ID删除对应的订单项信息。
     *
     * @param itemid 订单项ID。
     * @return 受影响的行数。
     */
    int deleteByPrimaryKey(Integer itemid);

    /**
     * 插入一条订单项信息记录。
     *
     * @param record 待插入的订单项信息。
     * @return 受影响的行数。
     */
    int insert(OrderItem record);

    /**
     * 插入一条订单项信息记录，只插入非空字段。
     *
     * @param record 待插入的订单项信息。
     * @return 受影响的行数。
     */
    int insertSelective(OrderItem record);

    /**
     * 根据指定的条件查询订单项信息列表。
     *
     * @param example 订单项条件的查询示例。
     * @return 订单项信息列表。
     */
    List<OrderItem> selectByExample(OrderItemExample example);

    /**
     * 根据订单项ID查询对应的订单项信息。
     *
     * @param itemid 订单项ID。
     * @return 订单项信息。
     */
    OrderItem selectByPrimaryKey(Integer itemid);

    /**
     * 根据指定的条件更新订单项信息，只更新非空字段。
     *
     * @param record  待更新的订单项信息。
     * @param example 订单项条件的查询示例。
     * @return 受影响的行数。
     */
    int updateByExampleSelective(@Param("record") OrderItem record, @Param("example") OrderItemExample example);

    /**
     * 根据指定的条件更新订单项信息。
     *
     * @param record  待更新的订单项信息。
     * @param example 订单项条件的查询示例。
     * @return 受影响的行数。
     */
    int updateByExample(@Param("record") OrderItem record, @Param("example") OrderItemExample example);

    /**
     * 根据订单项ID更新对应的订单项信息，只更新非空字段。
     *
     * @param record 待更新的订单项信息。
     * @return 受影响的行数。
     */
    int updateByPrimaryKeySelective(OrderItem record);

    /**
     * 根据订单项ID更新对应的订单项信息。
     *
     * @param record 待更新的订单项信息。
     * @return 受影响的行数。
     */
    int updateByPrimaryKey(OrderItem record);
}
