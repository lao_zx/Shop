package com.zhang.ssmschoolshop.dao;

import com.zhang.ssmschoolshop.entity.Address;
import com.zhang.ssmschoolshop.entity.AddressExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 与地址信息相关的数据库操作接口。
 */
public interface AddressMapper {

    /**
     * 根据指定条件统计符合条件的地址数量。
     *
     * @param example 地址条件的查询示例。
     * @return 地址数量。
     */
    long countByExample(AddressExample example);

    /**
     * 根据指定条件删除符合条件的地址信息。
     *
     * @param example 地址条件的查询示例。
     * @return 受影响的行数。
     */
    int deleteByExample(AddressExample example);

    /**
     * 根据地址ID删除对应的地址信息。
     *
     * @param addressid 地址ID。
     * @return 受影响的行数。
     */
    int deleteByPrimaryKey(Integer addressid);

    /**
     * 插入一条地址信息记录。
     *
     * @param record 待插入的地址信息。
     * @return 受影响的行数。
     */
    int insert(Address record);

    /**
     * 插入一条地址信息记录，只插入非空字段。
     *
     * @param record 待插入的地址信息。
     * @return 受影响的行数。
     */
    int insertSelective(Address record);

    /**
     * 根据指定条件查询符合条件的地址信息列表。
     *
     * @param example 地址条件的查询示例。
     * @return 地址信息列表。
     */
    List<Address> selectByExample(AddressExample example);

    /**
     * 根据地址ID查询对应的地址信息。
     *
     * @param addressid 地址ID。
     * @return 地址信息。
     */
    Address selectByPrimaryKey(Integer addressid);

    /**
     * 根据指定条件更新符合条件的地址信息，只更新非空字段。
     *
     * @param record  待更新的地址信息。
     * @param example 地址条件的查询示例。
     * @return 受影响的行数。
     */
    int updateByExampleSelective(@Param("record") Address record, @Param("example") AddressExample example);

    /**
     * 根据指定条件更新符合条件的地址信息。
     *
     * @param record  待更新的地址信息。
     * @param example 地址条件的查询示例。
     * @return 受影响的行数。
     */
    int updateByExample(@Param("record") Address record, @Param("example") AddressExample example);

    /**
     * 根据地址ID更新对应的地址信息，只更新非空字段。
     *
     * @param record 待更新的地址信息。
     * @return 受影响的行数。
     */
    int updateByPrimaryKeySelective(Address record);

    /**
     * 根据地址ID更新对应的地址信息。
     *
     * @param record 待更新的地址信息。
     * @return 受影响的行数。
     */
    int updateByPrimaryKey(Address record);
}
