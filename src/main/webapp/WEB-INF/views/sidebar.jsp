<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<div class="templatemo-sidebar" style="background: #ff720f">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style02.css">

    <header class="templatemo-site-header">
        <div class="fa fa-user fa-fw"></div>
        <h1>后台管理</h1>
    </header>
    <button class="back-button01" onclick="goBack()">返回上一页</button>

    <script>
        function goBack() {
            window.history.back();
        }
    </script>
    <div class="mobile-menu-icon">
        <i class="fa fa-bars"></i>
    </div>
    <nav class="templatemo-left-nav" style="background: #ff720f">
        <ul>
            <li><a href="${pageContext.request.contextPath}/admin/user/show"><i class="fa fa-user fa-fw"></i>用户管理</a></li>
            <li><a href="${pageContext.request.contextPath}/admin/goods/show"><i class="fa fa-bar-chart fa-fw"></i>商品管理</a></li>
            <li><a href="${pageContext.request.contextPath}/admin/order/send"><i class="fa fa-users fa-fw"></i>订单管理</a></li>
            <li><a href="${pageContext.request.contextPath}/admin/activity/show"><i class="fa fa-database fa-fw"></i>活动管理</a></li>
            <li><a href="${pageContext.request.contextPath}/admin/logout"><i class="fa fa-eject fa-fw"></i>退出系统</a></li>
        </ul>
    </nav>
    <br><br>
    <div>
        <div>
            <div>
                <div>
                    <div>
                        <a href="<c:url value='/' />" style="display: block; margin: 0 auto;" class="info-logo">
                            <img  style="display: block; margin: 0 auto; max-width: 50px; height: 50px;" src="<c:url value='/image/logo.jpg' />"/>
                        </a>
                        <br><br>

                        <p style="margin: 30px; text-align: center; color: whitesmoke;">
                            <%
                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy年MM月dd日HH点mm分");
                                String formattedDate = dateFormat.format(new Date());
                            %>
                            <time class="comment-time"><%= formattedDate %></time>
                            &copy; Powered By<br>
                            <a href="<c:url value='/' />" style="color: #0c79b1" rel="nofollow">瑟瑟小站</a>
                        </p>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
