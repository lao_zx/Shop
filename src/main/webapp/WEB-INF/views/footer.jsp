<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

    <!-- 其他 head 部分的内容可以保持不变 -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/font-awesome.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style02.css">
    <script src="<c:url value='/js/headroom.min.js' />"></script>
    <script src="<c:url value='/js/highlight.min.js' />"></script>
    <script src="<c:url value='/js/instantclick.min.js' />"></script>
<footer class="col-md-4" role="navigation"style="float: none">
    <div>
        <div>
            <div>
                <div>
                    <div>
                        <a href="<c:url value='/' />" style="display: block; margin: 0 auto;" class="info-logo">
                            <img  style="display: block; margin: 0 auto; max-width: 50px; height: 50px;" src="<c:url value='/image/logo.jpg' />"/>
                        </a>

                        <p style="margin: 30px; text-align: center; color: #ff6300;">
                            <%
                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy年MM月dd日HH点mm分");
                                String formattedDate = dateFormat.format(new Date());
                            %>
                            <time class="comment-time"><%= formattedDate %></time>
                            &copy; Powered By<br>
                            <a href="<c:url value='/' />" style="color: #ff6300" rel="nofollow">瑟瑟小站</a>
                        </p>

                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
