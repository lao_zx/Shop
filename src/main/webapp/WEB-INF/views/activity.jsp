<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- 页面元信息 -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>后台管理</title>
    <meta name="description" content="">
    <meta name="author" content="templatemo">

    <!-- 字体和样式 -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700' rel='stylesheet' type='text/css'>
    <link href="${pageContext.request.contextPath}/css/font-awesome.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/css/bootstrap/js/bootstrap.min.js"></script>
    <link href="${pageContext.request.contextPath}/css/templatemo-style.css" rel="stylesheet">

    <!-- HTML5 的 shim 和 Respond.js 以支持 IE8 中的 HTML5 元素和媒体查询 -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- SweetAlert 用于用户友好的警告提示 -->
    <script src="${pageContext.request.contextPath}/js/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/sweetalert.css">
</head>
<body>

<!-- 左侧栏 -->
<div class="templatemo-flex-row">
    <!-- 包含侧边栏 -->
    <jsp:include page="sidebar.jsp"/>

    <!-- 主要内容区域 -->
    <div class="templatemo-content col-1 light-gray-bg">
        <!-- 顶部导航容器 -->
        <div class="templatemo-top-nav-container">
            <div class="row">
                <!-- 顶部导航 -->
                <nav class="templatemo-top-nav col-lg-12 col-md-12">
                    <ul class="text-uppercase">
                        <!-- 导航链接 -->
                        <li><a href="${pageContext.request.contextPath}/admin/activity/show" class="active">所有活动</a></li>
                        <li><a href="${pageContext.request.contextPath}/admin/activity/add">添加活动</a></li>
                    </ul>
                </nav>
            </div>
        </div>

        <!-- 内容容器 -->
        <div class="templatemo-content-container">
            <!-- 无内边距的内容部件 -->
            <div class="templatemo-content-widget no-padding">
                <!-- 包含响应式表格的面板 -->
                <div class="panel panel-default table-responsive">
                    <!-- 带有标题的表格 -->
                    <table id="goodsinfo" class="table table-striped table-bordered templatemo-user-table">
                        <thead>
                        <tr>
                            <!-- 表头 -->
                            <td><a href="" class="white-text templatemo-sort-by">id<span class="caret"></span></a></td>
                            <td><a href="" class="white-text templatemo-sort-by">活动名<span class="caret"></span></a></td>
                            <td><a href="" class="white-text templatemo-sort-by">活动描述<span class="caret"></span></a></td>
                            <td><a href="" class="white-text templatemo-sort-by">折扣<span class="caret"></span></a></td>
                            <td><a href="" class="white-text templatemo-sort-by">满减<span class="caret"></span></a></td>
                            <td><a href="" class="white-text templatemo-sort-by">满免<span class="caret"></span></a></td>
                            <td>删除</td>
                        </tr>
                        </thead>
                        <tbody>
                        <!-- 循环遍历显示活动信息 -->
                        <c:forEach items="${pageInfo.list}" var="activity" varStatus="num">
                            <tr>
                                <!-- 行数据 -->
                                <td>${activity.activityid}</td>
                                <td>${activity.activityname}</td>
                                <td>${activity.activitydes}</td>
                                <td>${activity.discount}</td>
                                <c:if test="${!empty activity.fullprice}">
                                    <td>满${activity.fullprice}减${activity.reduceprice}</td>
                                </c:if>
                                <c:if test="${empty activity.fullprice}">
                                    <td>无满减</td>
                                </c:if>
                                <c:if test="${!empty activity.fullnum}">
                                    <td>满${activity.fullnum}免${activity.reducenum}</td>
                                </c:if>
                                <c:if test="${empty activity.fullnum}">
                                    <td>无满免</td>
                                </c:if>
                                <td><a href="${pageContext.request.contextPath}/admin/activity/delete?activityid=${activity.activityid}" class="templatemo-delete-btn" style="padding: 0;">删除</a></td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>

            <!-- 分页部分 -->
            <div class="pagination-wrap" id="page-div-nav">
                <div class="page-info" id="page-info-area">
                    <!-- 当前页、总页数和总记录数信息 -->
                    当前第${pageInfo.pageNum}页，总共${pageInfo.pages}页，总共${pageInfo.total}记录
                </div>
                <!-- 分页控件 -->
                <ul class="pagination">
                    <!-- 首页按钮 -->
                    <li>
                        <a href="${pageContext.request.contextPath}/admin/goods/show?page=1" aria-label="Next">
                            <span aria-hidden="true">首页</span>
                        </a>
                    </li>
                    <!-- 上一页按钮 -->
                    <c:if test="${pageInfo.hasPreviousPage}">
                        <li>
                            <a href="${pageContext.request.contextPath}/admin/goods/show?page=${pageInfo.pageNum - 1}" aria-label="Previous">
                                <span aria-hidden="true"><i class="fa fa-backward"></i></span>
                            </a>
                        </li>
                    </c:if>

                    <!-- 循环显示页码按钮 -->
                    <c:forEach items="${pageInfo.navigatepageNums}" var="pageNums">
                        <c:if test="${pageNums == pageInfo.pageNum}">
                            <!-- 当前页高亮显示 -->
                            <li class="active"><a href="${pageContext.request.contextPath}/admin/goods/show?page=${pageNums}">${pageNums}</a></li>
                        </c:if>
                        <c:if test="${pageNums != pageInfo.pageNum}">
                            <!-- 其他页码 -->
                            <li><a href="${pageContext.request.contextPath}/admin/goods/show?page=${pageNums}">${pageNums}</a></li>
                        </c:if>
                    </c:forEach>

                    <!-- 下一页按钮 -->
                    <c:if test="${pageInfo.hasNextPage}">
                        <li>
                            <a href="${pageContext.request.contextPath}/admin/goods/show?page=${pageInfo.pageNum + 1}" aria-label="Next">
                                <span aria-hidden="true"><i class="fa fa-forward"></i></span>
                            </a>
                        </li>
                    </c:if>
                    <!-- 末页按钮 -->
                    <li>
                        <a href="${pageContext.request.contextPath}/admin/goods/show?page=${pageInfo.pages}" aria-label="Next">
                            <span aria-hidden="true">末页</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div id="path" style="display: none;">${pageContext.request.contextPath}</div>
<!-- jQuery -->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/templatemo-script.js"></script>
<!-- Templatemo Script -->
<script>
    $(document).ready(function () {
        // Content widget with background image
        var imageUrl = $('img.content-bg-img').attr('src');
        $('.templatemo-content-img-bg').css('background-image', 'url(' + imageUrl + ')');
        $('img.content-bg-img').hide();
    });
</script>
</body>
</html>
