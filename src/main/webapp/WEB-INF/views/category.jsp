
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>瑟瑟商店</title>
    <script src="${pageContext.request.contextPath}/js/jquery.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap/css/bootstrap.min.css">
    <script src="${pageContext.request.contextPath}/css/bootstrap/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/sort.js"></script>
    <script src="${pageContext.request.contextPath}/js/holder.js"></script>
    <style>
        .like-button {
            left: 80% !important;
            top: 70% !important;
        }
        .data>ul {
            padding: 0;
        }
        .page-div {
            margin-top: 10px;
        }
        .page-info {
            padding: 35px 35px 35px 55px;
        }

        #backToTopBtn {
            position: fixed;
            bottom: 20px;
            right: 20px;
            background-color: #007BFF;
            color: #fff;
            border: none;
            border-radius: 5px;
            padding: 10px 20px;
            cursor: pointer;
            display: none;
        }

        #backToTopBtn:hover {
            background-color: #0056b3;
        }
    </style>
</head>
<body>
<div id="main" class="container">
    <div id="header">
        <jsp:include page="header.jsp"/>
    </div>
    <div class="content">
        <div class="module">
            <div class="">
                <h3>
                    类别><span style="color: red;">${cate}</span>
                </h3>
                <hr>
            </div>

            <div class="bd">
                <div class="data">
                    <ul>
                        <c:forEach items="${pageInfo.list}" var="goods">
                            <li class="data-item-li">
                                <div class="to-big">
                                    <a href="${pageContext.request.contextPath}/detail?goodsid=${goods.goodsid}"> <img src="${pageContext.request.contextPath}/pictures/${goods.imagePaths[0].path}" width="260px" height="260px" alt=""/>
                                    </a>
                                </div>
                                <p class="text-right">
                                    <a href="${pageContext.request.contextPath}/detail?goodsid=${goods.goodsid}">${goods.goodsname}</a>
                                </p>
                                <div class="text-right">
                                    <b>￥${goods.price}</b>
                                </div>
                                <div>
                                    <c:if test="${goods.fav}">
                                        <button
                                                class="like-button glyphicon glyphicon-heart btn btn-default"
                                                data-id="${goods.goodsid}"
                                                style="display: none;"></button>
                                    </c:if>
                                    <c:if test="${!goods.fav}">
                                        <button
                                                class="like-button glyphicon glyphicon-heart-empty btn btn-default"
                                                data-id="${goods.goodsid}"
                                                style="display: none;"></button>
                                    </c:if>
                                    <!-- <button class="like-button1 glyphicon glyphicon-heart-empty btn btn-default "></button> -->
                                </div>
                            </li>
                        </c:forEach>

                        <div class="clear-float" style="clear: both;"></div>
                    </ul>
                </div>
                <div class="row page-div">
                    <div class="col-md-5 page-info">
                        当前第${pageInfo.pageNum}页，共${pageInfo.pages}页，总共${pageInfo.size}条记录
                    </div>
                    <div class="col-md-6">
                        <nav aria-label="Page navigation">
                            <ul class="pagination pagination-lg">

                                <c:if test="${pageInfo.hasPreviousPage}">
                                    <li>
                                        <a href="${pageContext.request.contextPath}/search?keyword=${keyword}&page=${pageInfo.prePage}" aria-label="Previous">
                                            <span aria-hidden="true">&laquo;</span>
                                        </a>
                                    </li>
                                </c:if>

                                <c:if test="${!pageInfo.hasPreviousPage}">
                                    <li class="disabled">
                                        <a href="${pageContext.request.contextPath}/search?keyword=${keyword}&page=${pageInfo.prePage}" aria-label="Previous">
                                            <span aria-hidden="true">&laquo;</span>
                                        </a>
                                    </li>
                                </c:if>

                                <c:forEach items="${pageInfo.navigatepageNums}" var="pageNums">
                                    <c:if test="${pageNums == pageInfo.pageNum}">
                                        <li class="active"><a href="${pageContext.request.contextPath}/search?keyword=${keyword}&page=${pageNums}">${pageNums}</a></li>
                                    </c:if>
                                    <c:if test="${pageNums != pageInfo.pageNum}">
                                        <li><a href="${pageContext.request.contextPath}/search?keyword=${keyword}&page=${pageNums}">${pageNums}</a></li>
                                    </c:if>
                                </c:forEach>

                                <c:if test="${pageInfo.hasNextPage}">
                                    <li>
                                        <a href="${pageContext.request.contextPath}/search?keyword=${keyword}&page=${pageInfo.nextPage}" aria-label="Next">
                                            <span aria-hidden="true">&raquo;</span>
                                        </a>
                                    </li>
                                </c:if>

                                <c:if test="${!pageInfo.hasNextPage}">
                                    <li class="disabled">
                                        <a href="${pageContext.request.contextPath}/search?keyword=${keyword}&page=${pageInfo.nextPage}" aria-label="Next">
                                            <span aria-hidden="true">&raquo;</span>
                                        </a>
                                    </li>
                                </c:if>
                            </ul>
                        </nav>
                    </div>
                    <!-- 返回顶部按钮 -->
                    <button id="backToTopBtn" class="btn btn-default" style="display: none;">返回顶部</button>

                    <!-- JavaScript 代码 -->
                    <script>
                        $(document).ready(function () {
                            // 当页面向下滚动超过 200px 时，显示返回顶部按钮，否则隐藏
                            $(window).scroll(function () {
                                if ($(this).scrollTop() > 200) {
                                    $('#backToTopBtn').fadeIn();  // 显示按钮
                                } else {
                                    $('#backToTopBtn').fadeOut(); // 隐藏按钮
                                }
                            });

                            // 点击返回顶部按钮时，平滑滚动到页面顶部
                            $('#backToTopBtn').click(function () {
                                $('html, body').animate({scrollTop: 0}, 800); // 滚动到页面顶部，动画时长为 800 毫秒
                                return false;
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
<div align="center">
    <!-- 包含页脚部分 -->
    <%@ include file="/WEB-INF/views/footer.jsp" %>
</div>
</body>
</html>

