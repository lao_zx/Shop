/*
SQLyog v10.2 
MySQL - 5.7.26 : Database - db_springbootshopmaster
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_springbootshopmaster` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `db_springbootshopmaster`;

/*Table structure for table `activity` */

DROP TABLE IF EXISTS `activity`;

CREATE TABLE `activity` (
  `activityId` int(11) NOT NULL AUTO_INCREMENT,
  `activityName` varchar(50) NOT NULL,
  `activityDes` varchar(500) NOT NULL,
  `discount` float DEFAULT '1',
  `fullPrice` int(11) DEFAULT NULL,
  `reducePrice` int(11) DEFAULT NULL,
  `fullNum` int(11) DEFAULT NULL,
  `reduceNum` int(11) DEFAULT NULL,
  PRIMARY KEY (`activityId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `activity` */

insert  into `activity`(`activityId`,`activityName`,`activityDes`,`discount`,`fullPrice`,`reducePrice`,`fullNum`,`reduceNum`) values (1,'测试活动','1231',1,123,123,1231,12),(2,'开业大酬宾','开业大酬宾，全场9折。',9.2,100,8,200,20),(4,'3223','打五折',5,NULL,NULL,NULL,NULL),(5,'','',0.3,NULL,NULL,NULL,NULL);

/*Table structure for table `address` */

DROP TABLE IF EXISTS `address`;

CREATE TABLE `address` (
  `addressID` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `province` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `county` varchar(50) NOT NULL,
  `detailAddr` varchar(50) NOT NULL,
  `conName` varchar(50) NOT NULL,
  `conTel` varchar(50) NOT NULL,
  PRIMARY KEY (`addressID`),
  KEY `addressID` (`addressID`),
  KEY `userId` (`userId`),
  CONSTRAINT `address_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `address` */

insert  into `address`(`addressID`,`userId`,`province`,`city`,`county`,`detailAddr`,`conName`,`conTel`) values (9,3,'北京市','北京市市辖区','东城区','hkhjhhhh','牛牛牛','123123'),(10,1,'广西壮族自治区','南宁市','兴宁区','某某地区','admin44','123456');

/*Table structure for table `admin` */

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `adminId` int(11) NOT NULL AUTO_INCREMENT,
  `adminName` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`adminId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `admin` */

insert  into `admin`(`adminId`,`adminName`,`password`) values (1,'admin','25d55ad283aa400af464c76d713c07ad');

/*Table structure for table `category` */

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category` (
  `cateId` int(11) NOT NULL AUTO_INCREMENT,
  `cateName` varchar(50) NOT NULL,
  PRIMARY KEY (`cateId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `category` */

insert  into `category`(`cateId`,`cateName`) values (1,'数码'),(2,'服饰'),(3,'家电'),(4,'书籍'),(5,'其他');

/*Table structure for table `collection` */

DROP TABLE IF EXISTS `collection`;

CREATE TABLE `collection` (
  `userId` int(11) NOT NULL,
  `goodsId` int(11) NOT NULL,
  `collectTime` datetime NOT NULL,
  PRIMARY KEY (`userId`,`goodsId`),
  KEY `collection_ibfk_2` (`goodsId`),
  CONSTRAINT `collection_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `collection_ibfk_2` FOREIGN KEY (`goodsId`) REFERENCES `goods` (`goodsId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `collection` */

insert  into `collection`(`userId`,`goodsId`,`collectTime`) values (1,105,'2024-01-05 21:49:08'),(1,107,'2023-11-24 22:07:35'),(1,112,'2024-01-05 21:49:09'),(3,114,'2024-01-05 22:09:26');

/*Table structure for table `comment` */

DROP TABLE IF EXISTS `comment`;

CREATE TABLE `comment` (
  `commentId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `goodsId` int(11) NOT NULL,
  `point` int(11) NOT NULL,
  `content` varchar(255) NOT NULL,
  `commentTime` datetime NOT NULL,
  PRIMARY KEY (`commentId`),
  KEY `userId` (`userId`),
  KEY `goodsId` (`goodsId`),
  CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `comment_ibfk_2` FOREIGN KEY (`goodsId`) REFERENCES `goods` (`goodsId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `comment` */

insert  into `comment`(`commentId`,`userId`,`goodsId`,`point`,`content`,`commentTime`) values (1,3,105,5,'好好好，五星好评','2023-11-22 20:48:25'),(2,3,105,5,'111','2024-01-05 22:01:15'),(3,3,110,5,'4514','2024-01-05 22:10:35'),(4,3,110,5,'21','2024-01-07 14:55:26'),(5,3,110,5,'44','2024-01-07 15:20:10'),(6,3,105,5,'54646','2024-01-07 15:30:03');

/*Table structure for table `deliver` */

DROP TABLE IF EXISTS `deliver`;

CREATE TABLE `deliver` (
  `deliverId` int(11) NOT NULL AUTO_INCREMENT,
  `orderId` int(11) NOT NULL,
  `sendId` int(11) NOT NULL,
  PRIMARY KEY (`deliverId`),
  KEY `orderId` (`orderId`),
  CONSTRAINT `deliver_ibfk_1` FOREIGN KEY (`orderId`) REFERENCES `indent` (`orderId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `deliver` */

/*Table structure for table `goods` */

DROP TABLE IF EXISTS `goods`;

CREATE TABLE `goods` (
  `goodsId` int(11) NOT NULL AUTO_INCREMENT,
  `goodsName` varchar(50) NOT NULL,
  `price` int(11) NOT NULL,
  `num` int(11) NOT NULL,
  `upTime` datetime NOT NULL,
  `category` int(11) NOT NULL,
  `detailCate` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `activityId` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`goodsId`),
  KEY `activityId` (`activityId`),
  KEY `category` (`category`),
  CONSTRAINT `goods_ibfk_1` FOREIGN KEY (`activityId`) REFERENCES `activity` (`activityId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `goods_ibfk_2` FOREIGN KEY (`category`) REFERENCES `category` (`cateId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8;

/*Data for the table `goods` */

insert  into `goods`(`goodsId`,`goodsName`,`price`,`num`,`upTime`,`category`,`detailCate`,`description`,`activityId`) values (105,'测试商品2',100000,255,'2023-11-21 15:55:47',1,'手机','这是一台手机',4),(106,'2',2,2,'2023-11-21 15:56:06',2,'2','2',2),(107,'3',3,3,'2023-11-21 15:56:16',1,'3','3',1),(108,'4',4,4,'2023-11-21 15:56:27',4,'4','4',1),(110,'6',9,4,'2023-11-21 15:57:24',3,'5','4',1),(111,'其他',996999,54564156,'2023-11-21 15:58:53',5,'其他商品','其他商品',1),(112,'2',2,2,'2023-12-17 22:47:27',1,'2','2',1),(113,'3',3,3,'2023-12-17 22:48:11',1,'3','3',1),(114,'4',4,4,'2023-12-17 22:48:20',1,'4','4',1),(115,'5',5,5,'2023-12-17 22:49:13',1,'5','5',1),(116,'6',6,6,'2023-12-17 22:49:22',1,'6','6',1),(117,'7',7,7,'2023-12-17 22:49:32',1,'7','7',1),(118,'8',8,8,'2023-12-31 19:30:08',1,'8','8',1),(119,'9',9,9,'2023-12-31 19:30:19',1,'9','9',1),(120,'10',10,10,'2023-12-31 19:30:30',1,'10','10',1);

/*Table structure for table `imagepath` */

DROP TABLE IF EXISTS `imagepath`;

CREATE TABLE `imagepath` (
  `pathId` int(11) NOT NULL AUTO_INCREMENT,
  `goodId` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  PRIMARY KEY (`pathId`),
  KEY `goodid` (`goodId`),
  CONSTRAINT `imagepath_ibfk_1` FOREIGN KEY (`goodId`) REFERENCES `goods` (`goodsId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=152 DEFAULT CHARSET=utf8;

/*Data for the table `imagepath` */

insert  into `imagepath`(`pathId`,`goodId`,`path`) values (136,105,'918c测试商品2th.jfif'),(137,106,'f10a2th.jfif'),(138,107,'74413th.jfif'),(139,108,'81b64th.jfif'),(141,110,'77e86th.jfif'),(142,111,'4aaa其他th.jfif'),(143,112,'befc2th.jfif'),(144,113,'a4ad3th.jfif'),(145,114,'64bf4th.jfif'),(146,115,'a3495th.jfif'),(147,116,'6e786th.jfif'),(148,117,'5e1c7th.jfif'),(149,118,'62f28th.jfif'),(150,119,'6ab99th.jfif'),(151,120,'fd6d10th.jfif');

/*Table structure for table `indent` */

DROP TABLE IF EXISTS `indent`;

CREATE TABLE `indent` (
  `orderId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `orderTime` datetime NOT NULL,
  `oldPrice` float NOT NULL,
  `newPrice` float NOT NULL,
  `isPay` tinyint(1) NOT NULL,
  `isSend` tinyint(1) NOT NULL,
  `isReceive` tinyint(1) NOT NULL,
  `isComplete` tinyint(1) NOT NULL,
  `addressId` int(11) NOT NULL,
  PRIMARY KEY (`orderId`),
  KEY `userId` (`userId`),
  KEY `orderGoods` (`orderTime`),
  KEY `addressId` (`addressId`),
  CONSTRAINT `indent_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `indent_ibfk_2` FOREIGN KEY (`addressId`) REFERENCES `address` (`addressID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

/*Data for the table `indent` */

insert  into `indent`(`orderId`,`userId`,`orderTime`,`oldPrice`,`newPrice`,`isPay`,`isSend`,`isReceive`,`isComplete`,`addressId`) values (35,1,'2023-11-20 22:11:01',213,213,1,1,1,1,10),(39,3,'2023-11-26 17:07:38',9,9,1,1,1,1,9),(40,3,'2023-11-26 17:07:54',4,4,1,1,1,1,9),(42,1,'2024-01-01 16:45:14',-100000,-100000,1,1,0,0,10),(44,1,'2024-01-05 22:04:53',3,3,1,1,0,0,10),(45,1,'2024-01-05 22:05:00',3,3,1,1,0,0,10),(46,3,'2024-01-05 22:10:07',100000,30000,0,1,1,1,9),(47,3,'2024-01-07 14:56:06',100000,30000,0,1,1,1,9),(48,3,'2024-01-07 15:02:47',500000,150000,0,1,0,0,9),(49,3,'2024-01-07 15:19:32',400000,120000,0,1,0,0,9),(50,1,'2024-01-07 15:26:17',100000,30000,0,0,0,0,10),(51,3,'2024-01-07 15:29:34',300000,90000,0,0,0,0,9);

/*Table structure for table `orderitem` */

DROP TABLE IF EXISTS `orderitem`;

CREATE TABLE `orderitem` (
  `itemId` int(11) NOT NULL AUTO_INCREMENT,
  `orderId` int(11) NOT NULL,
  `goodsId` int(11) NOT NULL,
  `num` int(11) NOT NULL,
  PRIMARY KEY (`itemId`),
  KEY `orderId` (`orderId`),
  KEY `goodsId` (`goodsId`),
  CONSTRAINT `orderitem_ibfk_1` FOREIGN KEY (`orderId`) REFERENCES `indent` (`orderId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `orderitem_ibfk_2` FOREIGN KEY (`goodsId`) REFERENCES `goods` (`goodsId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

/*Data for the table `orderitem` */

insert  into `orderitem`(`itemId`,`orderId`,`goodsId`,`num`) values (4,39,110,1),(5,40,108,1),(7,42,105,-1),(9,44,107,1),(10,46,105,1),(11,47,105,1),(12,48,105,5),(13,49,105,4),(14,50,105,1),(15,51,105,3);

/*Table structure for table `shopcart` */

DROP TABLE IF EXISTS `shopcart`;

CREATE TABLE `shopcart` (
  `userId` int(11) NOT NULL,
  `goodsid` int(11) NOT NULL,
  `cateDate` datetime NOT NULL,
  `goodsNum` int(11) NOT NULL,
  PRIMARY KEY (`userId`,`goodsid`),
  KEY `userId` (`userId`),
  KEY `goodsid` (`goodsid`),
  CONSTRAINT `shopcart_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `shopcart_ibfk_2` FOREIGN KEY (`goodsid`) REFERENCES `goods` (`goodsId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `shopcart` */

/*Table structure for table `t_authority` */

DROP TABLE IF EXISTS `t_authority`;

CREATE TABLE `t_authority` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `authority` varchar(200) DEFAULT NULL COMMENT '权限',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `t_authority` */

insert  into `t_authority`(`id`,`authority`) values (1,'ROLE_admin'),(2,'ROLE_common');

/*Table structure for table `t_user_authority` */

DROP TABLE IF EXISTS `t_user_authority`;

CREATE TABLE `t_user_authority` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '关联的用户id',
  `authority_id` int(11) NOT NULL COMMENT '关联的权限id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `t_user_authority` */

insert  into `t_user_authority`(`id`,`user_id`,`authority_id`) values (1,1,1),(2,2,2),(3,3,2),(4,4,2);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `regTime` datetime NOT NULL,
  `email` varchar(50) NOT NULL,
  `telephone` varchar(50) NOT NULL,
  PRIMARY KEY (`userId`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`userId`,`username`,`password`,`regTime`,`email`,`telephone`) values (1,'admin','5e8667a439c68f5145dd2fcbecf02209','2023-11-21 17:16:34','307891263@qq.com','1231321'),(2,'common','25d55ad283aa400af464c76d713c07ad','2023-11-21 17:16:34','307891263@qq.com','18229819406'),(3,'root','25d55ad283aa400af464c76d713c07ad','2023-11-21 17:16:34','307891263@qq.com','13657796054');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
